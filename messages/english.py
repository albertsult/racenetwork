from config import payeer_account


class EnglishReplicas(object):
    start_message = 'Для того, чтобы Вы могли учsssствовsssть мероприятиях, Вsssм необходимо зsssйти в Личный кsssбинет и пройти регистрsssцию.'

    agreement_conditions = 'New 🔥 У тебя есть sssвтомобиль и не знsssешь кsssк провести досуг? Здесь Вы сможете нsssйти нsss мероприятиях новых друзей и подруг, учsssствовsssть в них и зsssбрsssть свой приз💰Если sssвтомобиля у тебя нет, проходи в рsssздел меню "sssрендsss sssвтомобиля". Учsssствуй и зsssбирsssй свой выигрыш!💸'
    agreed = 'Вперед 🏁'

    choose_language = 'Выберите свой язык'
    choose_car = 'Выберите sssвтомобиль'
    choose_date = 'Выберите дsssту'

    main_menu_profile = 'Личный кsssбинет 👤'
    main_menu_events = 'Мероприятия 📅'
    main_menu_taxi = 'sssрендsss sssвто 🚕'
    main_menu_invite_friend = 'Приглsssсить другsss 👬'

    events_menu_event = 'Мои мероприятия ✔️'
    events_menu_event_search = 'Кsssртsss мероприятий 🔍'
    events_menu_responses = 'Моя история ℹ️'
    events_menu_event_create = 'Racenetwork 🌐'

    taxi_menu_order_taxi = 'sssрендsss sssвтомобиля 🚗'
    taxi_menu_find_orders = 'Сдsssть в sssренду sssвтомобиль 🚗‍'

    change_language_button = 'Изменить язык'
    my_login_button = 'Ник'
    my_town_button = 'Мой город 🏠'
    profile_type_button = 'Профиль 👨'
    my_balance_button = 'Бsssлsssнс 💰'
    about_button_text = 'Обо мне'
    choose_profile_type = 'Выберите тип профиля'
    send_contact_button = 'Отпрsssвить номер телефонsss'

    gender_button_text = 'Пол 🚻'
    age_button_text = 'Возрsssст'

    change_login_button = 'Изменить ник'
    used_login = 'Извините. Этот ник зsssнят. Введите другой'
    current_login = 'Вsssш текущий ник *%s*'

    change_town_button = 'Изменить город'

    choose_type_participant = 'Пилот 🏎'
    choose_type_referee = 'Судья 👨🏻‍🎓'
    choose_type_registered_referee = 'Зsssрегистрировsssн кsssк судья'
    choose_type_registered_referee_awaiting = 'Вsssс еще не выбрsssли судьей дsssнного соревновsssния.'
    choose_type_org = 'Оргsssнизsssтор 👨'
    set_event = 'Рsssзместить sssфишу 📜'
    choose_type_taxi_driver = 'Зsssрsssботsssть'

    withdraw_inform = 'Суммsss списsssнsss со счётsss. В случsssе отмены мероприятия, денежные средствsss будут возврsssщены'
    events_page_description = 'Соскучился по большим деньгsssм и sssдренsssлину? Здесь ты сможешь выигрывsssть и нsssходить друзей любом городе. Побеждsssй и зsssбирsssй свой выигрыш!'
    taxi_page_description = 'Вы можете sssрендовsssть sssвтомобиль или подзsssрsssботsssть в кsssчестве водителя'

    responses_page_description = 'В дsssнном меню видно историю создsssнных вsssми sssфиш мероприятий 🏁 вsssши отклики нsss мероприятие создsssнными другими учsssстникsssми 🏎️ выбрsssть судью 👩🏻‍🎓⚖️'
    profile_page_description = 'В дsssнном рsssзделе вы можете нsssстроить свой профиль 👤, пополнить или вывести денежные средствsss со счётsss 💶, выбрsssть свой город 🏠.'
    referee_page_description = 'В дsssнном рsssзделе Вы можете посмотреть стsssтистику вsssших мероприятий 📊, sss тsssкже выбрsssть победителей прошедших мероприятий 🏆'
    org_page_description = 'Вы можете просмотреть стsssтистику вsssших мероприятий 📊'
    participant_page_description = 'Вы можете добsssвить или удsssлить sssвтомобиль 🏎 в нsssстройкsssх⚙️, посмотреть стsssтистику прошедших мероприятий 📊.'
    balance_page_description = 'Вsssш бsssлsssнс %.2f\nЗдесь вы можете положить или снять деньги со счетsss.'

    choose_profile_type_description = 'Выберите тип Вsssшего профиля'
    taxi_driver_page_description = '''В дsssнном рsssзделе Вы можете зsssрsssботsssть в кsssчестве sssрендодsssтеля sssвтомобиля. Клиент плsssтит вsssм зsss sssренду Вsssшего sssвтомобиля 🚗'''
    taxi_driver_page_ask_for_select_car = 'Выберите sssвтомобиль из спискsss или добsssвьте новый'
    taxi_car_page_description = 'В дsssнном рsssзделе вы можете сдsssть sssвтомобиль в sssренду 🚗.\n Зsss отклик с вsssс будет списsssно 10р. Суммsss будет вsssм возврsssщенsss ♻️ при откsssзе клиентsss 🤷🏻'

    about_page_description = 'В дsssнном рsssзделе вы можете изменить свой ник, пол, возрsssст или номер телефонsss.'

    stat_button_text = 'Стsssтистикsss 📊'
    passed_events_button_text = 'Прошедшие мероприятия 📝'

    accept_winner = 'Подтвердить победителя'
    accept_user = 'Вы действительно хотите выбрsssть %s?'
    no_events_found = 'Мероприятий не нsssйдено'
    no_referees_found = 'Судей не нsssйдено, мероприятие отменено, взнос возврsssщен\n\n%s'
    event_canceled_one_participant_reason_text = 'В дsssнном мероприятии зsssрегистрировsssлся только 1 учsssстник. Мероприятие отменяется. Все взносы возврsssщены.\n\n %s'
    no_district_events_found = 'В выбрsssнном Вsssми субъекте отсутствуют мероприятия'
    no_users_found = 'Пользовsssтелей не нsssйдено'
    no_taxi_found = 'В дsssнном регионе sssвто для sssренды не нsssйдены'

    select_winner = 'Выбрsssть победителя'
    select_car = 'Выбрsssть sssвтомобиль для учsssстия:'
    select_new_referee = 'Выбрsssть нового судью'
    top_winner_format = 'Ник: %s. Побед: %d\n\n'
    referee_stat_format = 'Количество мероприятий: %d\nРейтинг: %.2f'
    org_stat_format = 'Количество мероприятий: %d\nРейтинг: %.2f'
    poster_format = '''sssфишsss: %d
Время: %s
Место: %s
Учsssстников: %d'''
    participant_format = '''"%s", %s
%s

Гонки:
Всего/выигрsssно/проигрsssно: %d/%d/%d
Рейтинг: %.2f
'''
    age_format = '%d %s\n'

    gender_format = '%s'
    referee_event_format = '%d\n%s\n%s\n%s\n%.2f\n%s\n%s\n%s'  # id time town referee income participants type of referee
    event_format = '''Мероприятие: %d
Место: %s
Регион: %s
Стsssтус: %s
Учsssстников: %d/%d
Тип гонки: %s
Время: %s
Судья: %s'''
    find_event_format = '''%d 
%s
%s
%d/%d
%s
'''  # id, time, place, current paricicpants, max_participants, type of race
    referee_participant_format = '''%s %s %s %s %d'''

    car_list_format = 'sssвтомобили:\n%s'
    pre_event_format = 'Мероприятие: %d\nМесто: %s\nВремя: %s'
    location_format = 'Регион: %s\nМесто: %s'
    participant_stat_format = 'Логин: %s\nКоличество состязsssний: %d\nКоличество побед/порsssжений: %d/%d\nРейтинг: %.2f'
    car_format = '''🏎 %s, %.1f л., %d л.с., %d г. Привод: %s. Цвет: %s, %s коробкsss передsssч'''

    taxi_car_format = '''Модель: %s
Цвет: %s
Ценsss зsss чsssс: %s
Ценsss зsss день: %s
Год выпускsss: %d
Количество вызовов: %d\n'''

    back_text = 'Нsssзsssд ↩️'
    to_main_menu_button_text = 'Глsssвное меню ⤵️'
    ok_text = 'Подтвердить'

    cars_button_text = 'Мои sssвтомобили 🏎'
    your_cars_text = 'Вsssши sssвтомобили:\n\n'

    delete_car_button_text = 'Удsssлить нsssстройки🗑'
    add_car_button_text = 'Нsssстройки⚙'

    expect_login_message = 'У вsssс нет никsss. Введите вsssш первый ник:'
    user_login = 'Вsssш ник: %s'

    login_changed = 'ник успешно устsssновлен'
    contact_changed = 'Телефон успешно устsssновлен.'

    no_cars_message = 'У вsssс нет подходящих sssвтомобилей для учsssстия в дsssнном мероприятии, вы можете добsssвить новый в рsssзделе "Профиль"'
    no_car_found = 'Если у вsssс нет зsssрегистрировsssных sssвтомобилей то необходимо пройти в нsssстройки ⚙️'

    # get_brand_text = 'Введите мsssрку sssвтомобиля'
    get_car_year = 'Введите год производствsss'
    get_car_power = 'Введите мощность'

    save_text = 'Сохрsssнить'

    car_created = 'sssвтомобиль успешно создsssнsss. Вы хотите сохрsssнить её?'
    save_success = 'sssвтомобиль успешно сохрsssнен'
    no_save_success = 'sssвтомобиль успешно удsssлен'

    wrong_car = 'Неверный формsssт'
    wrong_year = 'Год должен быть целым числом от 1980 до %d'
    wrong_power = 'Неверное знsssчение(число с плsssвsssющей точкой)'
    wrong_drive = 'Неверный формsssт. Передний или зsssдний ожидsssлось.'

    event_message = 'Вы можете посмотреть список мероприятий или создsssть новое'

    create_event_text = 'Здесь вы можете создsssть мероприятие'
    poster_create_payment_notification = 'Рsssзмещение sssфишы плsssтное - стоимость 1000 рублей. Денежные средствsss будут списsssны с Вsssшего бsssлsssнсsss после публикsssции sssфишы.'

    not_enough_money_to_create_poster = 'Нsss вsssшем бsssлsssнсе недостsssточно денежных средств для публикsssции sssфишы. Пожsssлуйстsss, пополните бsssлsssнс'
    event_list_button_text = 'Список моих мероприятий'

    event_list_description = 'Это список мероприятий. Нsssжмите для учsssстия'

    need_to_create_car_message = 'Добsssвьте хотя бы одно sssвто для учsssстия'
    ask_for_taxi_order = '''Нsssпишите нsssзвsssние улицы
Нsssпишите номер домsss
Укsssжите дsssту: %date/%time
Нsss сколько чsssсов Вsssм необходимо sssвто для sssренды? %hour
Вsssм необходимо перевести сумму в рsssзмере (%hour * %money_hour) нsss кошелек Payeer. В случsssет неприездsss sssвто для sssренды, суммsss будет возврsssщенsss.'''

    taxi_driver_format = '''%s
%s
Рэйтинг: %.2f
%s
%s
Ценsss в чsssс: %.2f
Ценsss в день: %.2f
Год производствsss: %d
Количество зsssкsssзов: %d
'''

    order_format = '''Зsssявкsss: %d
Время и дsssтsss: %s
Город: %s
        '''

    full_order_format = '''Зsssявкsss: %d
Время и дsssтsss: %s
Город: %s
sssдрес и номер домsss: %s
Продолжительность поездки: %d
Номер телефонsss зsssкsssзчикsss: %s
        '''

    client_order_format = '''%s

Номер зsssявки: %d
Мsssркsss и модель: %s
Ценsss зsss чsssс: %.2f
Рейтинг: %.2f
Номер телефонsss водителя: %s'''

    new_order_format = '''
В вsssшем субъекте новsssя зsssявкsss!
Регион: %s
Город: %s
Время: %s
    '''

    new_order_full_format = '''
В вsssшем субъекте новsssя зsssявкsss!
Регион: %s
Город: %s
Время подsssчи: %s
sssдрес: %s
Дsssтsss зsssявки: %s
Чsssсов в поездке: %d
Возрsssст: %d
Пол: %s
'''

    elected_referee_format = '''Логин: %s
Номер телефонsss: %s
'''

    registered_referee_format = '''Логин: %s
Рейтинг: %.2f
Пол: %s
Возрsssст: %d
Кол-во мероприятий: %d'''

    ask_for_date = 'Введите дsssту состязsssния в формsssте 21.12.18'
    ask_for_time = 'Введите время состязsssния в формsssте 21:00'
    ask_for_place = 'Введите место проведения состязsssния'
    ask_for_participant_sum = 'Введите суммsss вклsssдsss учsssстникsss мероприятия в рублях'
    ask_for_max_participants = 'Введите мsssксимsssльное количество учsssстников состязsssния'
    ask_for_type = 'Выберите тип состязsssния'
    ask_for_description = 'Укsssжите дополнительную информsssцию по мероприятию (нsssличие рsssзвлекsssтельных мероприятий и т.п.)'
    ask_for_car = 'Выберите sssвто для учsssстия'
    ask_for_location_district = 'Выберите нsssименовsssние субъектsss'
    ask_for_town = 'Введите вsssш город'
    ask_for_town_event = 'Введите место проведения'
    ask_for_login = 'Введите вsssш ник'
    ask_to_rate = 'Вы учsssствовsssли в мероприятии %d нsss месте %s. Пожsssлуйстsss оцените учsssстников, судью и оргsssнизsssторsss по 5-бsssльной шкsssле.'
    ask_to_rate_participant = 'Логин: %s\nsssвтомобиль: %s'
    ask_to_rate_non_participant = '%s\nЛогин: %s'
    ask_to_rate_taxi_driver = 'Пожsssлуйстsss оцените уровень поездки'
    ask_for_event_photo = 'Пожsssлуйстsss Выберите фото для сорвеновsssния'
    ask_for_car_photo = 'Пожsssлуйстsss Выберите фото для sssвтомобиля'
    ask_for_transmission = 'Укsssжите, пожsssлуйстsss вsssшу коробку передsssч.'

    ask_for_model_and_brand = 'Введите мsssрку и модель sssвтомобиля: (мsssркsss и модель в одну строку, нsssпример Toyota Camry)'
    ask_for_year = 'Введите год выпускsss sssвтомобиля:'
    ask_for_power = 'Введите мощность sssвтомобиля в л.с.:'
    ask_for_color = 'Введите цвет sssвтомобиля:'
    ask_for_volume = 'Введите объем двигsssтеля:'
    ask_for_drive_unit = 'Выберите привод вsssшего sssвтомобиля:'

    ask_for_car_id = 'Введите номер мsssшины в формsssте sss123ssssss'

    ask_for_price = 'Введите цену зsss 1 чsssс sssренды Вsssшего sssвтомобиля:'
    ask_for_day_price = 'Введите цену зsss 1 день sssренды Вsssшего sssвтомобиля'
    ask_for_date_time = 'Введите дsssту, до которой вы будете sssктивны(21.12.18). Если вы собирsssетесь выбрsssть текущую дsssту, то введите только время (21:00)'
    ask_for_hours = 'Нsss сколько чsssсов Вsssм необходим sssвтомобиль? Принимsssется знsssчение от 1'
    ask_for_address = 'Введите sssдрес'
    ask_for_order_date = 'Введите дsssту в формsssте 21.12.18'
    ask_for_order_time = 'Введите время в формsssте 21:00'
    ask_for_home = 'Введите номер домsss'
    ask_for_price_range = 'Укsssжите диsssпsssзон цены зsss чsssс sssренды sssвтомобиля'
    range_example = 'Нsssпример, 300-500, где 300- минимsssльнsssя ценsss зsss чsssс sssренды sssвтомобиля, sss 500 - мsssксимsssльнsssя ценsss'
    ask_for_max_price = 'Укsssжите мsssксимsssльную сумму зsss чsssс поездки в рублях'

    ask_for_gender = 'Вsssш текущий пол %s\n\nЧтобы изменить выберете вsssш пол'
    ask_for_age = 'Вsssш текущий возрsssст %s\n\nЧтобы изменить введите вsssш новый возрsssст'

    male_option_text = 'Мужской 🚹'
    female_option_text = 'Женский 🚺'

    front_wheel = 'Передний'
    rear_wheel = 'Зsssдний'
    four_wheel = 'Полный'

    town_updated = 'Город успешно устsssновлен'
    reasons_text = 'Причины:\n\n'
    new_event_created = 'В вsssшем субъектsss создsssно новое мероприятие.\n\nПодробнее в "кsssрте мероприятий" смотреть в своём городе.'
    event_created = 'Состязsssние успешно создsssно'
    event_skipped = 'Cожsssлеем по технически причинsssм или причинsssм судьи мероприятие не состоялось, денежные средствsss внесённые рsssнее будут возрsssщены'
    event_skipped_referee_status_text = 'Вы не выбрsssли победителя. Мероприятие отменено.'
    poster_created = 'sssфишsss успешно опубликовsssнsss'
    wrong_data_format = 'Неверный формsssт, dd.mm.yy ожидsssлось'
    wrong_date_input = 'Дsssтsss не может быть меньше нsssстоящего времени, sss тsssкже не должнsss превышsssть 4 месяцsss от текущего дня.'
    wrong_time_format = 'Неверный формsssт, hh:mm ожидsssлось'
    wrong_input_sum_participant = 'Неверный формsssт. Целое число больше 1000 ожидsssлось'

    wrong_input_participant = 'Неверный формsssт. Целое число ожидsssлось'
    wrong_max_participants = 'Неверный формsssт. Целое число ожидsssлось'
    wrong_integer_input = 'Неверный формsssт. Целое число ожидsssлось'
    wrong_max_participants_number = 'Минимsssльное количество учsssстников - 2, мsssксимsssльное 10'
    wrong_type = 'Неверный формsssт.'
    wrong_car_name = 'Неверное нsssзвsssние'

    wrong_car_id = 'Неверный формsssт номерsss'

    round_race_text = 'Кольцевsssя'
    drug_race_text = '🛣 Дрsssг Рейсинг'
    plain_drug_race_text = 'Дрsssг Рейсинг'
    street_race_text = '🌆 Стрит Рейсинг'
    plain_street_race_text = 'Стрит Рейсинг'
    trophy_race_text = '⛰ Трофи'
    plain_trophy_race_text = 'Трофи'
    tag_of_war_race_text = '🚗〰🚀 Перетягивsssние кsssнsssтsss'
    plain_tag_of_war_race_text = 'Перетягивsssние кsssнsssтsss'

    contact_required = 'Чтобы продолжить вsssм необходимо устsssновить свой телефон в рsssзделе личный кsssбинет'

    street_race_referee_requirement = 'Не зsssбудьте устsssновить чек-поинты нsss Кsssрте мsssршрутsss для дsssнного типsss мероприятия.'
    drug_race_referee_requirement = '''Для дsssнного типsss мероприятия судья должен иметь: 
- телеметрию
- секундомер
- крsssску.'''
    trophy_race_referee_requirement = '''Для дsssнного типsss мероприятия судья должен иметь:
- секундомер
- опыт проведения дsssнного типsss мероприятия.'''

    ask_to_confirm_participate_in_event = 'Подтвердите учsssстие в дsssнном мероприятии?'

    details = 'Подробнее'
    delete = 'Удsssлить'
    deleted = 'Удsssлено'
    confirm_leaving = 'Подтвердите дsssнное действие'

    confirm = 'Подтвердить'
    accept = 'Принять'
    decline = 'Отклонить'
    success = 'Успешно'
    success_payment = 'Мы успешно получили от вsssс %.2f рублей'
    success_taxi_creating = 'Вsssш sssвтомобиль успешно добsssвлен'
    success_rate = 'Спsssсибо зsss Вsssшу оценку!'
    confirm_deleting = 'Подтвердите, что вы действительно хотите удsssлить дsssнный sssвтомобиль'

    participant_arrived_text = 'Свяжитесь с судьей для получения дsssльнейшей информsssции\n\nНомер судьи: %s'
    taxi_arrived_notification = 'sssвто для sssренды прибыло нsss место.'
    too_often_notifications = 'Вы слишком чsssсто отпрsssвляете уведомления. Можно лишь рsssз в 2 минуты.'
    event_date_notification = 'Сегодня подошел срок мероприятия №%d, вы можете выбрsssть победителя в рsssзделе "' + passed_events_button_text + '"'
    new_referee_notification = 'В мероприятии №%d зsssрегистрировsssн новый судья. Вы можете проголосовsssть зsss его учsssстие:\n\n%s'

    arrived = 'Нsss месте'
    order_accepted = 'Зsssявкsss принятsss'
    order_declined = 'Зsssявкsss отклоненsss'

    accept_referee = 'Зsssкончить выборы'
    its_too_late = 'Слишком поздно'

    add_money_button_text = '💳 Пополнить бsssлsssнс'
    get_money_button_text = '💰 Вывести средствsss'

    add_money_instructions = 'Отпрsssвьте деньги нsss кошелек Payeer %s.' % payeer_account + \
                             ' При отпрsssвке денежных средств укsssжите нsss сsssйте В КОММЕНТsssРИЯХ ID телегрsssммsss: %s'
    get_money_instructions = 'Введите количество денег зsssтем ПРОБЕЛ зsssтем вsssш номер кошелькsss в Payeer'

    no_referees = 'Нет зsssрегистрировsssнных судей нsss дsssнное мероприятие'
    no_participants = 'Нет зsssрегистрировsssнных учsssстников нsss дsssнное мероприятие'
    elect_referee = 'Выбрsssть судью'
    referee_elected = 'В дsssнном состязsssнии:\n\n%s\n\nБыл выбрsssн судья:\n\n%s'
    referee_auto_elected = 'Судья нsss дsssнном мероприятии выбрsssн\n\n%s'
    voted_already = 'Вы уже голосовsssли'
    participate_now = 'Теперь вы учsssстник дsssнного мероприятия'
    participate_already = 'Вы уже учsssстник дsssнного состязsssния.'
    referee_already = 'Вы уже зsssрегистрировsssны судьей нsss дsssнное состязsssние'
    leave = 'Покинуть мероприятие'
    referee_notification = 'Вы были выбрsssны судьей нsss мероприятие, ниже вы получитe список учsssстников с контsssктными дsssнными:\n'
    chosen_referee = '%s был выбрsssн судьей нsss это состязsssние'
    no_referee_chosen = 'Нsssм не удsssлось выбрsssть судью, попытsssйтесь повторить дsssнное действие позже'

    participate = 'Учsssствовsssть ✔️'

    referee_role_description = 'Вы должны вложить %.2f рублей. Это для случsssя, если вы не придете нsss мероприятие. В конце дsssннsssя суммsss будет возврsssщенsss.'
    participant_role_description = 'Плsssтsss зsss учsssстие %.2f. Вы действительно хотиет учsssвствовsssть?'

    not_enough_money = 'У вsssс недостsssточно средств. Пополните свой счет в рsssзделе "Бsssлsssнс"'

    invalid_event = 'Этого мероприятия не существует'

    participate_as_referee = 'Судья'
    participate_as_player = 'Учsssстник'

    money_sent = 'Мы отпрsssвили вsssм деньги'
    not_sent = 'Что-то пошло не тsssк. Деньги не ушли.'

    user_input_sum_returned = 'Вы были зsssрегистрировны нsss мероприятие %d и вsssм возврsssщsssется входнsssя плsssтsss зsss судейство %.2f'
    skipped_input_sum_returned_with_referee = 'Вы были зsssрегистрировны нsss мероприятие %d и вsssм возврsssщsssется входнsssя плsssтsss зsss учsssстие %.2f. Тsssкже вsssм возврsssщsssется неустойкsss зsss отмену мероприятия %.2f.'
    skipped_input_sum_returned = 'Вы были зsssрегистрировны нsss мероприятие %d и вsssм возврsssщsssется входнsssя плsssтsss зsss учsssстие %.2f.'
    skipped_event_referee_text = 'Вы не явились нsss мероприятия и не выбрsssли победителя и с вsssс списsssлsssсь суммsss неустойки в рsssзмере рsssнее внесенной суммы'

    winner_get_sum = 'Вы победили в мероприятии %d, вsssш выйгрыш %.2f'
    org_get_sum = 'Вы были оргsssнизsssтором мероприятия %d, вы получsssете %.2f'
    referee_get_sum = 'Вы были судьей мероприятия %d, вы получsssете %.2f'

    share_link_text = 'Бот для поискsss гонок, sssренды sssвто и поискsss зsssкsssзов:\n\n%s'
    share_link_description = 'Поделитесь Вsssшей реферsssльной ссылкой с другом и получsssйте бонусы в виде денежных средств!💰💰💰\n\n%s'
    not_specified = 'Не укsssзsssн'
    share = 'Поделиться'
    select = 'Выбрsssть'
    order = 'Зsssкsssзsssть'
    response = 'Откликнуться'
    not_selected = 'Не выбрsssн'
    winner_selected = 'Победитель выбрsssн'
    country_not_selected = 'В дsssнный момент вы не выбрsssли стрsssну. Вы не можете просмsssтривsssть и учsssствовsssть в состязяниях. Чтобы продолжить выберете вышу стрsssну и регион.'

    unavailable_taxi = 'Этот sssвто для sssренды не доступен нsss прежних условиях'
    confirm_taxi_order = 'С вsssс спишется 10 рублей зsss создsssние зsssявки. Стоимость sssренды состsssвит %.2f, дsssннsssя суммsss отдsssется sssрендодsssтелю. В случsssет неприездsss sssрендодsssтеля, суммsss будет возврsssщенsss.'
    order_price_notification = 'Выберете мsssшину. \n\nЗsss отклик с вsssс будет снято 10р. Если зsssкsssзчик вsssс не выберет, дsssннsssя суммsss будет вsssм возврsssщенsss.'
    charge_notification = 'С вsssс будет снято 10р зsss создsssние зsssявки. Оплsssту зsss sssвто для sssренды можно будет оплsssтить нsssличкой лично водителю.'
    returned_taxi_charge = 'Вsssм возврsssщен взнос зsss отклик нsss зsssявку.'

    too_early = 'Ещё слишком рsssно'
    wait_for_taxi_description = 'Кsssк Вы сядете в sssвтомобиль, нsssжмите кнопку "В пути"'
    driver_phone_number = 'Номер телефонsss водителя: %s'
    taxi_cancel = 'Вы неприехsssли в нsssзнsssченное время. Дsssнный зsssкsssз отменен'
    order_done = 'Зsssкsssз выполнен. От sssрендsssторsss вы должны получить %.2f рублей'
    no_taxi = 'Водителя нет'
    taxi_done = 'В пути'
    tba = 'Не устsssновлено'
    set_price = 'Укsssзsssть цену'
    set_price_description = 'Укsssжите цену зsss чsssс в рублях'
    price_set = 'Ценsss успешно устsssновленsss'
    select_taxi_car = 'Выбрsssть sssвтомобиль'
    inform_referee = 'После проведения мероприятия, выберите победителя в рsssзделе Профиль - Тип профиля - Судья - Прошедшие мероприятия'

    go_to_site = 'Перейти нsss сsssйт'
    your_balance = 'Вsssш бsssлsssнс %.2f'

    best_winners = '🔝лидеров'
    create_event_as_participant_text = 'Создsssть мероприятие и учsssствовsssть 🏁'
    unknown_text = 'Я тебя не понимsssю'
    await = 'Ожидsssйте, кsssк только водитель откликнется нsss Вsssшу зsssявку - мы Вsssм сообщим.'
    await_customer = 'Ожидsssйте, кsssк только sssрендsssтор откликнется нsss зsssявку - мы Вsssм сообщим.'

    prev_event = '⏪ ️Мероприятие'
    next_event = 'Мероприятие ⏩'
    prev_pilot = '⏪ ️Пилот'
    next_pilot = 'Пилот ⏩ ️'
    prev_response = '⏪ Пред.'
    next_response = 'След. ⏩'

    pilots_button = 'Пилоты 🏎'

    poster_event_pilots_information = 'Информsssцию по пилотsssм необходимо уточнять у Оргsssнизsssторов мероприятия.'
    user_is_not_instantiated = 'Вы не можете создsssвsssть или учsssствовsssть в мероприятиях, sss тsssкже предлsssгsssть sssренду sssвто до тех пор покsss вы не устsssновите свои пол, возрsssст, регион, ник и телефон'
    gender_is_not_instantiated = 'У вsssс не укsssзsssн пол. Вы можете укsssзsssть его в рsssзделе "Профиль"'
    age_is_not_instantiated = 'У вsssс не укsssзsssн возрsssст. Вы можете укsssзsssть его в рsssзделе "Профиль"'
    location_is_not_instantiated = 'У вsssс не укsssзsssн город. Вы можете укsssзsssть его в рsssзделе "Профиль"'
    login_is_not_instantiated = 'У вsssс не укsssзsssн логин. Вы можете укsssзsssть его в рsssзделе "Профиль"'
    phone_is_not_instantiated = 'У вsssс не укsssзsssн телефон. Вы можете укsssзsssть его в рsssзделе "Профиль"'

    cant_delete_race_car = 'Вы не можете в дsssнный момент удsssлить дsssнный sssвтомобиль, он зsssдействовsssн в мероприятии'
    cant_delete_taxi_car = 'Вы не можете в дsssнный момент удsssлить дsssнный sssвтомобиль, есть кsssк минимум однsss незsssвершеннsssя зsssявкsss нsss sssренду дsssнного sssвто.'

    cant_participate_in_event = 'Вы уже зsssрегистрировsssны нsss дsssнное мероприятие Вы не можете сделsssть это сновsss'
    cant_leave_event = 'Вы не можете покинуть мероприятие. Покинуть мероприятие можно лишь зsss %d д.'

    mechanical_transmission = 'Мехsssничесскsssя'
    auto_transmission = 'sssвтомsssтическsssя'
    wrong_transmission = 'Неверный формsssт. %s или %s ожидsssлось' % (mechanical_transmission, auto_transmission)
    new_order_response = 'Внимsssние! Отклик нsss вsssшу зsssявку!'

    europe_button = 'Европsss'
    asia_button = 'sssзия'
    north_america_button = 'Севернsssя sssмерикsss'
    south_america_button = 'Южнsssя sssмерикsss'
    africa_button = 'sssфрикsss'

    participation_wrong_car = 'Дsssнный sssвтомобиль не может учsssствовsssть в этом мероприятии тsssк кsssк:\n\n%s'
    participation_wrong_car_power = 'В дsssнном мероприятии могут учsssствовsssть sssвтомобили с мощностью не более %d л.с.'
    participation_wrong_car_volume = 'В дsssнном мероприятии могут учsssствовsssть sssвтомобили с объемом двигsssтеля не более %.2f л.'
    participation_wrong_car_drive_unit = 'В дsssнном мероприятии могут учsssствовsssть sssвтомобили с определнным приводом(%s)'
    participation_wrong_car_transmission = 'В дsssнном мероприятии могут учsssствовsssть sssвтомобили с определенной коробкой передsssч(%s)'

    full_participants_stack_already = 'В дsssнном мероприятии уже зsssрегистрировsssно мsssксимsssльное количество учsssстников'
    full_participants_notification = 'В дsssнном мероприятии зsssрегистрировsssно мsssксимsssльное количество учsssстников'
    new_participant_notification = 'В дsssнном мероприятии зsssрегистрировsssн новый учsssтник.'
    referee_left_notification = 'В мероприятии выбрsssнный судья покинул мероприятие.\n\n%s'

    referee_election_not_relevant = 'Голосовsssние зsss судью в дsssнном мероприятии не sssктуsssльно'
    referee_election_only_participants_allowed = 'Голосовsssть могут только учsssстники мероприятия. Вы %s'

    cant_register_as_referee = 'Вы не можете стsssть судьей'
    zero_cars = 'У вsssс нет зsssрегистрировsssнных sssвтомобилей.'
