from .english import EnglishReplicas
from .russian import RussianReplicas

rus = 'Русский 🇷🇺'
eng = 'English 🇬🇧'
messages = {
    rus: RussianReplicas(),
    eng: EnglishReplicas()
}
