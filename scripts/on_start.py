import os
import sys

import requests
from requests.auth import HTTPBasicAuth
sys.path.append('/app/')
from config import token

host = os.environ.get('HOST')
certificate_needed = os.environ.get('CERTIFICATE')
from bot import bot

if host:
    bot.remove_webhook()
    if certificate_needed:
        bot.set_webhook('https://' + host + '/%s' % token, certificate=open('./certs/public.pem'))
    else:
        bot.set_webhook('https://' + host + '/%s' % token)

