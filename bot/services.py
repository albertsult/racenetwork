from django.db.models import Q, Count, F

from bot.models import Event
from bot.utils.utils import local_now


def get_responses_list(user):
    events = Event.objects.filter(
        Q(participantinevent__participant=user.participant) |
        Q(registeredreferee__referee=user.referee) |
        Q(creator=user.organiser)). \
        filter(time__gt=local_now()). \
        exclude(created=False, finished=True). \
        distinct(). \
        order_by('id'). \
        all()
    # responses = list(set(responses))
    return events


def get_passed_events_list(user):
    events = Event.objects.filter((Q(referee=user.referee) | (
                Q(registeredreferee__referee=user.referee) & Q(time__gt=local_now())) & Q(
        created=True))).distinct().all()
    return events


def get_list_of_available_events(user, district):
    return Event.objects. \
        annotate(participants_count=Count('participantinevent')). \
        filter(time__gt=local_now()). \
        filter(Q(participants_count__lt=F('max_participants')) | Q(referee=None),
               created=True,
               district=district,
               time__gt=local_now(),
               ). \
        exclude(registeredreferee__referee__user_id=user.id). \
        exclude(participantinevent__participant__user__id=user.id)
