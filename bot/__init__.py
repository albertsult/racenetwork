import telebot
from config import token

bot = telebot.TeleBot(token)

delete_message = bot.delete_message
send_message = bot.send_message


def alt_send_message(*args, corrupt_exceptions=False, **kwargs):
    try:
        return send_message(*args, **kwargs)
    except:
        if not corrupt_exceptions:
            raise


def alt_delete_message(*args, corrupt_exceptions=False, **kwargs):
    try:
        return delete_message(*args, **kwargs)
    except:
        if not corrupt_exceptions:
            raise


bot.delete_message = alt_delete_message
bot.send_message = alt_send_message