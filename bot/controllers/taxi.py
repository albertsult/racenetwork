from django.core.exceptions import ObjectDoesNotExist
from telebot import types

from bot import bot
from bot.controllers.payeer import send_money
from bot.models import TaxiDriver, TaxiOrder
from bot.utils.utils import local_now
from config import payeer_income_account
from race_bot import settings


def notify_taxi_drivers(created_order: TaxiOrder):
    taxi_drivers = TaxiDriver.objects.filter(user__district=created_order.district)
    if not settings.DEBUG:
        taxi_drivers = taxi_drivers.exclude(user=created_order.user)
    drivers_to_notify = []
    for driver in taxi_drivers:
        cars = driver.user.taxi_cars.filter(created=True).all()
        for car in cars:
            try:
                if created_order.min_price <= car.price <= created_order.max_price:
                    drivers_to_notify.append(driver)
                    break
            except ObjectDoesNotExist:
                pass

    for driver in drivers_to_notify:
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(driver.user.replicas.details, callback_data='order_taxi.details.%d' % created_order.id))
        bot.send_message(driver.user.id, created_order.format(driver.user, spec='driver_notification_short'),
                         reply_markup=markup, corrupt_exceptions=True)


def return_charges():
    orders = TaxiOrder.objects\
        .filter(accepted=False, finished=False, time__lt=local_now())\
        .exclude(created=None)
    # bot.send_message(121193878, str(orders))
    for order in orders:
        drivers = order.payed_drivers.all()
        # bot.send_message(121193878, str(drivers))
        for driver in drivers:
            driver.balance += 10
            driver.save()
            bot.send_message(driver.id, driver.replicas.returned_taxi_charge)
        order.finished = True
        order.save()


def finish_order(order, message, user):
    driver = order.car.driver
    income_amount = order.total_price
    bot.send_message(driver.id, driver.replicas.order_done % income_amount)

    markup = types.InlineKeyboardMarkup(row_width=1)
    markup.add(
        types.InlineKeyboardButton('⭐️⭐⭐️⭐️⭐️️', callback_data='rate.taxi_driver.5.%d' % order.id),
        types.InlineKeyboardButton('⭐️⭐⭐️⭐️', callback_data='rate.taxi_driver.4.%d' % order.id),
        types.InlineKeyboardButton('⭐️⭐⭐️', callback_data='rate.taxi_driver.3.%d' % order.id),
        types.InlineKeyboardButton('⭐️⭐️', callback_data='rate.taxi_driver.2.%d' % order.id),
        types.InlineKeyboardButton('⭐️', callback_data='rate.taxi_driver.1.%d' % order.id), )

    bot.edit_message_text(user.replicas.ask_to_rate_taxi_driver, user.id, message.message_id, reply_markup=markup)

    order.finished = True
    order.save()
    send_money(payeer_income_account, 20)
