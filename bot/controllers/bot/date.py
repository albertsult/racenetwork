import locale
from calendar import monthrange
import datetime
import telebot
from telebot import types

from bot import bot
from bot.controllers.bot.event import ask_for_time
from bot.models import User
from bot.utils.to_page_actions import to_choose_date


def date_callback(user: User, message: telebot.types.Message, data):
    topic, data = data.split('.', 1)
    if topic == 'year':
        year = data
        ask_about_month(user, year, message)

    elif topic == 'month':
        year, month = data.split('.')
        day_nums = monthrange(int(year), int(month))
        markup = types.InlineKeyboardMarkup(row_width=7)
        buttons = []

        for i in range(0, day_nums[0]):
            buttons.append(types.InlineKeyboardButton('_', callback_data='choose_date.day'))
        for i in range(1, day_nums[1]+1):
            buttons.append(types.InlineKeyboardButton(i, callback_data='choose_date.day.%s.%s.%d' % (year, month, i)))
        markup.add(*buttons)
        markup.add(types.InlineKeyboardButton(user.replicas.back_text, callback_data='choose_date.back.day.%s.%s' % (year, month)))

        bot.edit_message_text(user.replicas.choose_date + '\n\n%s.%s' % (month, year), user.id, message.message_id,
                              reply_markup=markup)
    elif topic == 'day':
        year, month, day = data.split('.')
        date = datetime.datetime(int(year), int(month), int(day)).date()
        if datetime.datetime.now().date() <= date < datetime.datetime.now().date() + datetime.timedelta(days=30 * 4):

            if user.state == 32:
                user.creating_event.time = user.creating_event.time.replace(year=date.year, month=date.month,
                                                                            day=date.day)
                user.creating_event.save()
                ask_for_time(user)

            elif user.state == 115:
                user.creating_order.time = user.creating_order.time.replace(year=date.year, month=date.month,
                                                                            day=date.day)
                user.creating_order.save()

                user.state = 116
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                markup.add(types.KeyboardButton(user.replicas.back_text),
                           types.KeyboardButton(user.replicas.to_main_menu_button_text))
                bot.send_message(user.id, user.replicas.ask_for_order_time)
            bot.delete_message(user.id, message.message_id)
        else:
            bot.send_message(user.id, user.replicas.wrong_date_input)
            to_choose_date(user)
    elif topic == 'back':
        topic, data = data.split('.', 1)
        if topic == 'month':
            markup = types.InlineKeyboardMarkup(row_width=5)
            markup.add(types.InlineKeyboardButton('2018', callback_data='choose_date.year.%d' % 2018),
                       types.InlineKeyboardButton('2019', callback_data='choose_date.year.%d' % 2019),
                       types.InlineKeyboardButton('2020', callback_data='choose_date.year.%d' % 2020),
                       types.InlineKeyboardButton('2021', callback_data='choose_date.year.%d' % 2021),
                       types.InlineKeyboardButton('2022', callback_data='choose_date.year.%d' % 2022), )
            bot.edit_message_text(user.replicas.choose_date, user.id, message.message_id, reply_markup=markup)

        elif topic == 'day':
            year = data.split('.')[0]
            ask_about_month(user, year, message)


def ask_about_month(user, year, message):
    markup = types.InlineKeyboardMarkup(row_width=3)
    buttons = []
    if user.lang != 'English':
        locale.setlocale(locale.LC_ALL, 'ru_RU.utf-8')
    buttons.append(types.InlineKeyboardButton('_', callback_data='choose_date.month'))

    for i in range(1, 13):
        month = datetime.date(2008, i, 1).strftime('%B')
        buttons.append(types.InlineKeyboardButton(month, callback_data='choose_date.month.%s.%d' % (year, i)))
    markup.add(*buttons)
    markup.add(types.InlineKeyboardButton(user.replicas.back_text, callback_data='choose_date.back.month.%s' % year))
    bot.edit_message_text(user.replicas.choose_date + '\n\n%s' % year, user.id, message.message_id,
                          reply_markup=markup)
