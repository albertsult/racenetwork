import datetime

from django.utils import timezone
from telebot import types

from bot import bot
from bot.controllers.bot.event import ask_for_time
from bot.controllers.taxi import notify_taxi_drivers, finish_order
from bot.models import User, RaceCar, TaxiOrder, TaxiCar, Region
from bot.utils.bot_utils import process_district_input, process_continent_input, process_country_input
from bot.utils.to_page_actions import to_main_page, to_balance_page, to_choose_date, to_choose_region, ask_for_phone
from bot.utils.utils import local_now


def order_taxi_callback(user: User, message: types.Message, data):
    topic, data = data.split('.', 1)
    if topic == 'details':
        order = TaxiOrder.objects.get(id=data)
        if not order.accepted:
            answer = order.format(user, spec='driver_notification_full')
            markup = types.InlineKeyboardMarkup()
            markup.add(
                types.InlineKeyboardButton(user.replicas.confirm, callback_data='order_taxi.accept.%d' % order.id),
                types.InlineKeyboardButton(user.replicas.decline, callback_data='order_taxi.decline.%d' % order.id))
            bot.edit_message_text(answer, user.id, message.message_id, reply_markup=markup)
        else:
            bot.delete_message(user.id, message.message_id)

    elif topic == 'accept':
        order = TaxiOrder.objects.get(id=data)
        if not order.accepted:
            cars = user.taxi_cars.filter(created=True).filter(price__gte=order.min_price,
                                                              price__lte=order.max_price).all()
            bot.send_message(user.id, user.replicas.order_price_notification)
            for car in cars:
                markup = types.InlineKeyboardMarkup()
                markup.add(types.InlineKeyboardButton(user.replicas.select_taxi_car,
                                                      callback_data='order_taxi.select_car.%d.%d' % (car.id, order.id)))
                bot.send_message(user.id, car.format(user.replicas), reply_markup=markup)
            bot.delete_message(user.id, message.message_id)
        else:
            bot.delete_message(user.id, message.message_id)
    elif topic == 'decline':
        bot.delete_message(user.id, message.message_id)
    elif topic == 'select_car':
        data = data.split('.')
        car = TaxiCar.objects.get(id=data[0])
        order = TaxiOrder.objects.get(id=data[1])
        if user.phone:
            if not order.accepted:
                if user.balance >= 10:
                    user.balance -= 10
                    order.payed_drivers.add(user)
                    order.save()
                    buttons = [types.InlineKeyboardButton(order.user.replicas.confirm,
                                                          callback_data='order_taxi.accept_car.%d.%d' % (
                                                              car.id, order.id)),
                               types.InlineKeyboardButton(order.user.replicas.decline,
                                                          callback_data='order_taxi.decline_car.%d.%d' % (
                                                              car.id, order.id))]
                    car.send_to_client(order.user, buttons)

                else:
                    bot.edit_message_text(user.replicas.not_enough_money, user.id, message.message_id)
                    to_balance_page(user)
                bot.delete_message(user.id, message.message_id)

        else:
            ask_for_phone(user)
    elif topic == 'accept_car':
        data = data.split('.')
        car = TaxiCar.objects.get(id=data[0])
        order = TaxiOrder.objects.get(id=data[1])
        if not order.accepted:
            total_price = order.get_price(car)
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton(user.replicas.ok_text,
                                                  callback_data='order_taxi.real_accept.%d.%d.%d' % (
                                                      car.id, order.id, total_price)))
            bot.send_message(user.id, user.replicas.your_balance % user.balance)
            bot.send_message(user.id, user.replicas.confirm_taxi_order % total_price,
                             reply_markup=markup)
        else:
            bot.delete_message(user.id, message.message_id)

    elif topic == 'decline_car':
        data = data.split('.')
        car = TaxiCar.objects.get(id=data[0])
        order = TaxiOrder.objects.get(id=data[1])
        if not order.finished:
            driver = car.driver
            if driver in order.payed_drivers.all():
                driver.balance += 10
                order.payed_drivers.remove(driver)
                order.save()
                bot.send_message(car.driver.id, car.driver.replicas.returned_taxi_charge)
            bot.edit_message_text(user.replicas.order_declined, user.id, message.message_id)
        else:
            bot.delete_message(user.id, message.message_id)
    elif topic == 'real_accept':
        data = data.split('.')
        car = TaxiCar.objects.get(id=data[0])
        order = TaxiOrder.objects.get(id=data[1])
        total_price = int(data[2])
        if not order.accepted:
            order.total_price = total_price
            order.car = car
            order.accepted = True
            order.save()
            if user.balance >= 10:
                user.balance -= 10
                user.save()

                payed_drivers = order.payed_drivers.exclude(id=order.car.driver.id)
                for driver in payed_drivers:
                    driver.balance += 10
                    driver.save()
                    bot.send_message(driver.id, driver.replicas.returned_taxi_charge)

                # order.car.driver.balance -= 10
                # order.car.driver.save()

                markup = types.InlineKeyboardMarkup()
                markup.add(
                    types.InlineKeyboardButton(user.replicas.no_taxi, callback_data='order_taxi.no_taxi.%d' % order.id),
                    types.InlineKeyboardButton(user.replicas.taxi_done,
                                               callback_data='order_taxi.done_taxi.%d' % order.id), )
                bot.edit_message_text(user.replicas.wait_for_taxi_description + '\n\n' +
                                      user.replicas.driver_phone_number % order.car.driver.phone,
                                      user.id,
                                      message.message_id, reply_markup=markup)

                markup = types.InlineKeyboardMarkup()
                markup.add(
                    types.InlineKeyboardButton(user.replicas.arrived,
                                               callback_data='order_taxi.taxi_arrived.%d' % order.id))
                bot.send_message(order.car.driver.id,
                                 order.car.driver.replicas.order_accepted + '\n\n' + order.format(
                                     order.car.driver,
                                     spec='full'),
                                 reply_markup=markup)
            else:
                bot.edit_message_text(user.replicas.not_enough_money, user.id, message.message_id)
                to_balance_page(user)
            # if user.balance >= 10:
            #
            # else:
            #     bot.edit_message_text(user.replicas.not_enough_money, user.id, message.message_id)
            #     to_balance_page(user)
        else:
            bot.delete_message(user.id, message.message_id)

    elif topic == 'no_taxi':
        order = TaxiOrder.objects.get(id=data)
        if not order.finished:
            if order.time.replace(tzinfo=None) < datetime.datetime.now():

                bot.send_message(order.car.driver.id, order.car.driver.replicas.taxi_cancel + '\n\n' +
                                 order.format(order.car.driver, spec='full'))
                user.balance += 10
                order.finished = True
                order.save()
                bot.edit_message_text(order.format(user, spec='client', message=user.replicas.success), user.id, message.message_id)
            else:
                too_early(user, order, message)

        else:
            bot.edit_message_text(user.replicas.order_declined, user.id, message.message_id)

    elif topic == 'done_taxi':
        order = TaxiOrder.objects.get(id=data)
        if not order.finished:
            if order.time.replace(tzinfo=None) < datetime.datetime.now():
                finish_order(order, message, user)
                return
            else:
                too_early(user, order, message)
        else:
            bot.edit_message_text(user.replicas.order_declined, user.id, message.message_id)

    elif topic == 'taxi_arrived':
        order = TaxiOrder.objects.get(id=data)
        if not order.finished:
            if order.time.replace(tzinfo=None) < datetime.datetime.now():
                if order.last_notification:
                    if order.last_notification < local_now() - datetime.timedelta(minutes=2):
                        bot.send_message(order.user.id, order.user.replicas.taxi_arrived_notification)
                        order.last_notification = local_now()
                    else:
                        bot.send_message(user.id, user.replicas.too_often_notifications)
                else:
                    bot.send_message(order.user.id, order.user.replicas.taxi_arrived_notification)
                    order.last_notification = local_now()
                order.save()
            else:
                too_early(user, order, message, client=False)

        else:
            bot.edit_message_text(user.replicas.order_declined, user.id, message.message_id)
    user.save()


def taxi_action(user, message):
    # elif user.state < 110:
    #     order_taxi_action(user, message)
    if user.state >= 110:
        find_taxi_action(user, message)


def find_taxi_action(user: User, message: types.Message):
    if message.text == user.replicas.to_main_menu_button_text:
        user.creating_order.delete()
        user.creating_order = None
        to_main_page(user)
    elif user.state == 110:
        process_continent_input(user, message, to_main_page, next_state=110.5)
    elif user.state == 110.5:
        process_country_input(user, message, to_main_page, next_state=111)
    elif user.state == 111:
        process_district_input(user, message, to_main_page, district_order_action, next_state=112,
                               need_to_main_menu=True)
    elif user.state == 112:
        if message.text != user.replicas.back_text:
            user.creating_order.town = message.text
            user.state = 113
            bot.send_message(user.id, user.replicas.ask_for_address)
        else:
            to_choose_region(user, state=111)

    elif user.state == 113:
        if not message.text == user.replicas.back_text:
            user.creating_order.address = message.text
            user.state = 115
            to_choose_date(user)

            bot.send_message(user.id, user.replicas.ask_for_order_date)
        else:
            user.state = 112
            bot.send_message(user.id, user.replicas.ask_for_town)
    # elif user.state == 114:
    #     if not message.text == user.replicas.back_text:
    #         if not message.text.startswith('0') and message.text[0].isdigit():
    #             user.creating_order.home_number = message.text
    #             user.state = 115
    #             to_choose_date(user)
    #             # bot.send_message(user.id, user.replicas.ask_for_order_date)
    #         else:
    #             bot.send_message(user.id, user.replicas.wrong_type)
    #     else:
    #         user.state = 113
    #         bot.send_message(user.id, user.replicas.ask_for_address)

    elif user.state == 115:
        if not message.text == user.replicas.back_text:
            try:
                date = datetime.datetime.strptime(message.text, '%d.%m.%y').date()
                user.creating_order.time = user.creating_order.time.replace(year=date.year, month=date.month,
                                                                            day=date.day)
                user.state = 116
                bot.send_message(user.id, user.replicas.ask_for_order_time)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_data_format)
        else:
            user.state = 114
            bot.send_message(user.id, user.replicas.ask_for_home)

    elif user.state == 116:
        if not message.text == user.replicas.back_text:
            try:
                time = datetime.datetime.strptime(message.text, '%H:%M').time()
                user.creating_order.time = user.creating_order.time.replace(hour=time.hour, minute=time.minute)
                user.state = 117
                bot.send_message(user.id, user.replicas.ask_for_hours)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_time_format)
        else:
            user.state = 115
            bot.send_message(user.id, user.replicas.ask_for_order_date)
    elif user.state == 117:
        if not message.text == user.replicas.back_text:
            try:
                hours = int(message.text)
                if hours >= 1:
                    user.creating_order.hours = hours
                    user.state = 118
                    bot.send_message(user.id, user.replicas.ask_for_price_range)
                    bot.send_message(user.id, user.replicas.range_example)
                    bot.send_message(user.id, user.replicas.charge_notification)

                else:
                    bot.send_message(user.id, user.replicas.wrong_input_participant)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_input_participant)
        else:
            user.state = 116
            bot.send_message(user.id, user.replicas.ask_for_order_time)
    elif user.state == 118:
        if not message.text == user.replicas.back_text:
            try:
                if user.balance >= 10:

                    min_price, max_price = message.text.split('-')
                    min_price = float(min_price)
                    max_price = float(max_price)
                    if max_price > min_price > 0:
                        user.creating_order.min_price = min_price
                        user.creating_order.max_price = max_price

                        user.creating_order.created = timezone.now()
                        user.creating_order.save()
                        user.balance -= 10
                        notify_taxi_drivers(user.creating_order)
                        send_timer(user)
                        to_main_page(user, user.replicas.await)
                    else:
                        bot.send_message(user.id, user.replicas.wrong_input_participant)
                else:
                    bot.send_message(user.id, user.replicas.not_enough_money)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_input_participant)
        else:
            user.state = 117
            bot.send_message(user.id, user.replicas.ask_for_hours)

    user.creating_order.save()


def district_order_action(user, district: Region):
    user.creating_order.district = district


def back_to_region(user):
    to_choose_region(user, state=111)


def send_timer(user):
    try:
        with open('static/countdown.gif', 'rb') as file:
            # content = file.read()
            bot.send_document(user.id, file)
    except Exception:
        import traceback
        traceback.print_exc()


def too_early(user, order, message, client=True):
    if client:
        markup = types.InlineKeyboardMarkup()
        markup.add(
            types.InlineKeyboardButton(user.replicas.no_taxi, callback_data='order_taxi.no_taxi.%d' % order.id),
            types.InlineKeyboardButton(user.replicas.taxi_done,
                                       callback_data='order_taxi.done_taxi.%d' % order.id), )
        bot.edit_message_text(user.replicas.too_early + '\n\n' +
                              user.replicas.driver_phone_number % order.car.driver.phone,
                              user.id,
                              message.message_id,
                              reply_markup=markup)
    else:
        markup = types.InlineKeyboardMarkup()
        markup.add(
            types.InlineKeyboardButton(user.replicas.arrived,
                                       callback_data='order_taxi.taxi_arrived.%d' % order.id))
        bot.edit_message_text(user.replicas.too_early + '\n\n' + order.car.driver.replicas.order_accepted + '\n\n' + order.format(
                             order.car.driver, spec='full'), order.car.driver.id, message.message_id,
                         reply_markup=markup)
