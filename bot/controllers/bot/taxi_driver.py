from django.utils import timezone
from telebot import types

from bot import bot
from bot.controllers.bot.taxi import send_timer
from bot.models import User, TaxiCar, TaxiOrder
from bot.utils.to_page_actions import to_main_page, to_taxi_driver_page, to_create_taxi_car_page, \
    to_profile_page


def taxi_driver_callback(user: User, message: types.Message, data):
    topic, data = data.split('.', 1)
    if topic == 'car':
        topic, data = data.split('.', 1)
        if topic == 'delete':
            car = TaxiCar.objects.filter(id=data).first()
            if car:
                if car.can_deleted():
                    markup = types.InlineKeyboardMarkup()
                    markup.add(types.InlineKeyboardButton(user.replicas.confirm,
                                                          callback_data='taxi.car.confirm_delete.%d' % car.id))
                    bot.edit_message_text(user.replicas.confirm_deleting + '\n\n' + car.brand, user.id,
                                          message.message_id, reply_markup=markup)
                else:
                    bot.edit_message_text(user.replicas.cant_delete_taxi_car, user.id, message.message_id)
            else:
                bot.delete_message(user.id, message.message_id)
        elif topic == 'confirm_delete':
            car = TaxiCar.objects.filter(id=data).first()
            if car:
                if car.can_deleted():
                    car.delete()
                    bot.edit_message_text(user.replicas.success, user.id, message.message_id)
                else:
                    bot.edit_message_text(user.replicas.cant_delete_taxi_car, user.id, message.message_id)

            else:
                bot.delete_message(user.id, message.message_id)


def taxi_driver_action(user: User, message: types.Message):
    if user.state == 120:
        if message.text == user.replicas.add_car_button_text:
            if user.instantiated():
                to_create_taxi_car_page(user)

        elif message.text == user.replicas.back_text:
            to_profile_page(user)
        elif message.text == user.replicas.taxi_menu_find_orders:
            to_taxi_driver_page(user,
                                offer_car=False,
                                add_car=False,
                                delete_button=False)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)
    elif 121 <= user.state <= 126:
        create_taxi_action(user, message)



def create_taxi_action(user: User, message: types.Message):
    if message.text == user.replicas.to_main_menu_button_text:
        user.creating_taxi_car.delete()
        user.creating_taxi_car = None
        to_main_page(user)
    elif message.text == user.replicas.back_text:
        to_taxi_driver_page(user, offer_car=False, add_car=True, select_button=True)
    else:
        if user.state == 121:
            user.creating_taxi_car.brand = message.text
            user.state = 122
            bot.send_message(user.id, user.replicas.ask_for_price)
        elif user.state == 122:
            try:
                user.creating_taxi_car.price = float(message.text)
                user.state = 123
                bot.send_message(user.id, user.replicas.ask_for_day_price)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_input_participant)
        elif user.state == 123:
            try:
                user.creating_taxi_car.day_price = float(message.text)
                user.state = 124
                # bot.send_message(user.id, user.replicas.ask_for_color)
                bot.send_message(user.id, user.replicas.ask_for_car_photo)

            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_input_participant)
        elif user.state == 124:
            if message.photo or (message.document and message.document.thumb):
                if message.photo:
                    user.creating_taxi_car.photo_id = message.photo[2].file_id
                elif message.document and message.document.thumb:
                    user.creating_taxi_car.photo_id = message.document.file_id
                user.state = 125
                bot.send_message(user.id, user.replicas.ask_for_year)
            else:
                bot.send_message(user.id, user.replicas.wrong_type)
        elif user.state == 125:
            current_year = timezone.now().year
            try:
                year = int(message.text)
                if 1980 <= year <= current_year:
                    user.creating_taxi_car.prod_year = year
                else:
                    raise ValueError()
                user.state = 126
                bot.send_message(user.id, user.replicas.ask_for_color)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_year % current_year)
        elif user.state == 126:
            user.creating_taxi_car.color = message.text
            # user.state = 125
            # bot.send_message(user.id, user.replicas.ask_for_car_id)
            user.creating_taxi_car.created = True
            user.creating_taxi_car.save()
            bot.send_message(user.id, user.replicas.await_customer)
            send_timer(user)
            to_taxi_driver_page(user, [user.replicas.success_taxi_creating], select_button=True, offer_car=False)
        user.creating_taxi_car.save()


def find_taxi_orders(user, district):
    orders = TaxiOrder.objects \
        .filter(max_price__gte=user.find_orders_car.price,
                min_price__lte=user.find_orders_car.price,
                district=district,
                time__gt=timezone.localtime(timezone.now()),
                accepted=False) \
        .all()
    if orders:
        for order in orders:
            markup = types.InlineKeyboardMarkup()
            markup.add(
                types.InlineKeyboardButton(user.replicas.details, callback_data='find_orders.details.%d' % order.id))
            bot.send_message(user.id, order.format(user, spec='short'), reply_markup=markup)
    else:
        bot.send_message(user.id, user.replicas.no_users_found)
    to_main_page(user)
