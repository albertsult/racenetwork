import telebot
from telebot import types

from bot import bot
from bot.controllers.bot.date import date_callback
from bot.controllers.bot.event import find_event_callback, rate_callback, event_action, \
    winners_callback, events_page_action
from bot.controllers.bot.organiser import org_page_action
from bot.controllers.bot.participant import participant_page_action, participant_callback
from bot.controllers.bot.profile_action import profile_page_action, balance_page_action
from bot.controllers.bot.referee import referee_page_action, referee_callback
from bot.controllers.bot.response import response_callback
from bot.controllers.bot.taxi import taxi_action, order_taxi_callback
from bot.controllers.bot.taxi_driver import taxi_driver_callback, taxi_driver_action
from bot.controllers.payment import payment_callback
from bot.controllers.user import change_language
from bot.models import User, RaceCar, Event, TaxiOrder
from bot.utils.bot_utils import unknown_action
from bot.utils.to_page_actions import to_profile_page, to_main_page, to_register_in_event, \
    to_choose_date, to_events_page, to_agreement_page, to_taxi_driver_page, to_choose_region, ask_for_phone


def callback(message, data, user):
    topic, data = data.split('.', 1)
    if topic == 'referee':
        referee_callback(message, data, user)
        return
    elif topic == 'race_car':
        participant_callback(user, message, data)
    elif topic == 'responses':
        response_callback(user, message, data)
    elif topic == 'find':
        find_event_callback(user, message, data)
    elif topic == 'rate':
        rate_callback(user, message, data)
    elif topic == 'taxi':
        taxi_driver_callback(user, message, data)
    elif topic == 'create_event':
        if user.state == 31:
            topic, data = data.split('.', 1)
            if topic == 'car':
                car = RaceCar.objects.filter(id=data).first()
                if car:
                    participation = user.creating_event.participantinevent_set.filter(
                        participant__user__id=user.id).first()
                    participation.car = car
                    participation.save()
                    user.state = 32
                    user.save()
                    to_choose_date(user)
                    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                    markup.add(types.KeyboardButton(user.replicas.back_text),
                               types.KeyboardButton(user.replicas.to_main_menu_button_text))
                    bot.send_message(user.id, '✔️', reply_markup=markup)

                else:
                    bot.delete_message(user.id, message.message_id)
    elif topic == 'order_taxi':
        order_taxi_callback(user, message, data)
        return
    elif topic == 'choose_date':
        date_callback(user, message, data)
    elif topic == 'payment':
        payment_callback(user, message, data)
    elif topic == 'winners':
        winners_callback(user, message, data)
    else:
        event = Event.objects.get(id=data)
        to_register_in_event(user, event)
    user.save()


def existed_user_action(user: User, message: telebot.types.Message):
    if user.state == 0:
        status = change_language(user, message.text)
        if status:
            to_agreement_page(user)
    elif 1 <= user.state < 10:
        main_menu(user, message)
    elif 10 <= user.state < 20:
        profile_page_action(user, message)
    elif 20 <= user.state < 50:
        event_action(user, message)
    elif 60 <= user.state < 70:
        referee_page_action(user, message)
    elif 70 <= user.state < 80:
        org_page_action(user, message)
    elif 80 <= user.state < 90:
        participant_page_action(user, message)
    elif 90 <= user.state < 100:
        balance_page_action(user, message)
    elif 100 <= user.state < 120:
        taxi_action(user, message)
    elif 120 <= user.state < 130:
        taxi_driver_action(user, message)
    elif 130 <= user.state < 140:
        profile_page_action(user, message)  # In interval 10-20 no more place for states. Here is additional place
    user.save()


def main_menu(user: User, message: telebot.types.Message):
    if user.state == 1:
        main_menu_action(user, message)
    elif user.state == 2:
        events_page_action(user, message)
    elif user.state == 3:
        taxi_page_action(user, message)
    elif user.state == 4:
        if message.text == user.replicas.agreed:
            to_main_page(user)


def main_menu_action(user: User, message: telebot.types.Message):
    if message.text == user.replicas.main_menu_profile:
        to_profile_page(user)
    elif message.text == user.replicas.main_menu_events:
        to_events_page(user)
    elif message.text == user.replicas.taxi_menu_order_taxi:
        if user.instantiated():
            order = TaxiOrder(user=user)
            order.save()
            user.creating_order = order
            to_choose_region(user, state=111, choose_continent_state=110)
    elif message.text == user.replicas.main_menu_invite_friend:
        link_text = 'https://t.me/%s?start=%d' % (bot.get_me().username, user.id)
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(user.replicas.share,
                                              switch_inline_query=user.replicas.share_link_text % link_text))
        bot.send_message(user.id, user.replicas.share_link_description % link_text, reply_markup=markup)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
    else:
        unknown_action(user)


def taxi_page_action(user: User, message: telebot.types.Message):
    if message.text == user.replicas.taxi_menu_find_orders:
        to_taxi_driver_page(user,
                            messages_list=[user.replicas.taxi_driver_page_description, user.replicas.taxi_driver_page_ask_for_select_car],
                            delete_button=False)
    elif message.text == user.replicas.back_text:
        to_main_page(user)

