import pytz
import telebot
from telebot import types

from bot import bot
from bot.controllers.event import end_of_event
from bot.models import User, Event, Participant
from bot.services import get_passed_events_list
from bot.utils.bot_utils import gen_referee_stat, send_passed_event_action
from bot.utils.to_page_actions import to_select_winner_message, to_profile_page, \
    to_main_page
from bot.utils.utils import local_now
from messages import messages as mes
from race_bot.settings import TIME_ZONE

tz = pytz.timezone(TIME_ZONE)


def referee_callback(message, data, user):
    topic, data = data.split('.', 1)
    if topic == 'select_event':
        event = Event.objects.get(id=data)
        if not event.skipped:
            to_select_winner_message(user, message, event)
        else:
            bot.edit_message_text(user.replicas.skipped_event_referee_text, user.id, message.message_id)

    elif topic == 'select_winner':
        data = data.split('.')
        event = Event.objects.get(id=data[0])
        if not event.skipped:
            winner = Participant.objects.get(id=data[1])
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton(mes[user.lang].accept_winner,
                                                  callback_data='referee.accept_winner.%d.%d' % (event.id, winner.id)))
            bot.edit_message_text(mes[user.lang].accept_user % winner.user.login, user.id, message.message_id,
                                  reply_markup=markup)
        else:
            bot.edit_message_text(user.replicas.skipped_event_referee_text, user.id, message.message_id)
    elif topic == 'accept_winner':
        data = data.split('.')
        event = Event.objects.get(id=data[0])
        winner = Participant.objects.get(id=data[1])
        bot.edit_message_text(user.replicas.success, user.id, message.message_id)
        end_of_event(event, winner)


def referee_page_action(user: User, message: telebot.types.Message):
    if user.state == 60:
        referee_main_page_action(user, message)


def referee_main_page_action(user: User, message: telebot.types.Message):
    if message.text == mes[user.lang].stat_button_text:
        answer = gen_referee_stat(user)
        bot.send_message(user.id, answer)
    elif message.text == mes[user.lang].passed_events_button_text:
        events = get_passed_events_list(user)
        send_passed_event_action(user, events)
    elif message.text == mes[user.lang].back_text:
        to_profile_page(user)
    elif message.text == mes[user.lang].to_main_menu_button_text:
        to_main_page(user)
