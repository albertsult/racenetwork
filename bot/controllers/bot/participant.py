import telebot
from django.utils import timezone
from telebot import types

from bot import bot
from bot.models import User, RaceCar, Event
from bot.utils.to_page_actions import to_participant_page, to_main_page, \
    to_create_race_car_page, to_profile_page
from bot.utils.utils import local_now


def participant_callback(user, message, data):
    topic, data = data.split('.', 1)
    if topic == 'delete':
        car = RaceCar.objects.get(id=data)
        events = Event.objects.filter(participantinevent__car=car, time__gt=local_now(), finished=False, created=True)
        if not events.count() > 0:
            text = user.replicas.confirm_deleting + '\n\n%s' % car.brand
            markup = types.InlineKeyboardMarkup()
            markup.add(
                types.InlineKeyboardButton(user.replicas.confirm, callback_data='race_car.confirm_delete.%d' % car.id))
            bot.edit_message_text(text, user.id, message.message_id, reply_markup=markup)
        else:
            bot.edit_message_text(user.replicas.cant_delete_race_car, user.id, message.message_id)
    elif topic == 'confirm_delete':
        car = RaceCar.objects.get(id=data)
        events = Event.objects.filter(participantinevent__car=car, time__gt=local_now(), finished=False, created=True)
        if not events.count() > 0:
            car.delete()
            text = user.replicas.deleted
            bot.edit_message_text(text, user.id, message.message_id)
        else:
            bot.edit_message_text(user.replicas.cant_delete_race_car, user.id, message.message_id)


def participant_page_action(user: User, message: telebot.types.Message):
    if user.state == 80:
        participant_main_page_action(user, message)
    elif user.state == 81:
        cars_page_action(user, message)
    elif 82 <= user.state <= 89:
        create_car_action(user, message)


def participant_main_page_action(user: User, message: telebot.types.Message):
    if message.text == user.replicas.stat_button_text:
        all_patricipation = user.participant.participantinevent_set.exclude(event__winner=None).count()
        wins = user.participant.winner_set.count()
        answer = user.replicas.participant_stat_format % (user.login, all_patricipation, wins, all_patricipation-wins, user.participant.rating)
        bot.send_message(user.id, answer)
    elif message.text == user.replicas.add_car_button_text:
        to_create_race_car_page(user)
    # elif message.text == user.replicas.choose_type_taxi_driver:
    #     to_taxi_driver_page(user, select_button=False, messages=[user.replicas.taxi_car_page_description])
    elif message.text == user.replicas.back_text:
        to_profile_page(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)


def cars_page_action(user: User, message: telebot.types.Message):
    if message.text == user.replicas.add_car_button_text:
        to_create_race_car_page(user)
    elif message.text == user.replicas.delete_car_button_text:
        cars = user.race_cars.filter(created=True).all()
        for car in cars:
            answer = car.format(user.replicas)
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton(user.replicas.delete, callback_data='race_car.delete.%d' % car.id))
            bot.send_message(user.id, answer, reply_markup=markup)
    elif message.text == user.replicas.back_text:
        to_participant_page(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)


def create_car_action(user: User, message: telebot.types.Message):
    if message.text == user.replicas.to_main_menu_button_text:
        user.creating_race_car.delete()
        user.creating_race_car = None
        to_main_page(user)
    elif message.text == user.replicas.back_text:
        to_participant_page(user)
    else:
        if user.state == 82:
            try:
                user.creating_race_car.brand = message.text
                user.state = 83
                bot.send_message(user.id, user.replicas.ask_for_year)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_type)
        elif user.state == 83:
            current_year = timezone.now().year
            try:
                year = int(message.text)
                if 1980 <= year <= current_year:
                    user.creating_race_car.prod_year = year
                else:
                    raise ValueError()
                user.state = 84
                bot.send_message(user.id, user.replicas.ask_for_power)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_year % current_year)
        elif user.state == 84:
            try:
                user.creating_race_car.power = float(message.text)
                user.state = 85
                bot.send_message(user.id, user.replicas.ask_for_color)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_power)
        elif user.state == 85:
            user.creating_race_car.color = message.text
            user.state = 86
            bot.send_message(user.id, user.replicas.ask_for_volume)
        elif user.state == 86:
            try:
                user.creating_race_car.engine_volume = float(message.text)
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                markup.add(types.KeyboardButton(user.replicas.front_wheel),
                           types.KeyboardButton(user.replicas.rear_wheel),
                           types.KeyboardButton(user.replicas.four_wheel),
                           types.KeyboardButton(user.replicas.to_main_menu_button_text))
                user.state = 87
                bot.send_message(user.id, user.replicas.ask_for_drive_unit, reply_markup=markup)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_type)
        elif user.state == 87:
            try:
                if message.text == user.replicas.front_wheel:
                    user.creating_race_car.drive_unit = 0
                elif message.text == user.replicas.rear_wheel:
                    user.creating_race_car.drive_unit = 1
                elif message.text == user.replicas.four_wheel:
                    user.creating_race_car.drive_unit = 2
                else:
                    raise ValueError()
                user.state = 88
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                markup.add(types.KeyboardButton(user.replicas.mechanical_transmission),
                           types.KeyboardButton(user.replicas.auto_transmission),
                           types.KeyboardButton(user.replicas.to_main_menu_button_text))
                bot.send_message(user.id, user.replicas.ask_for_transmission, reply_markup=markup)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_drive)
        elif user.state == 88:
            if message.text == user.replicas.mechanical_transmission or message.text == user.replicas.auto_transmission:
                user.creating_race_car.transmission = 0 if message.text == user.replicas.mechanical_transmission else 1
                user.state = 89
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                markup.add(types.KeyboardButton(user.replicas.to_main_menu_button_text))
                bot.send_message(user.id, user.replicas.ask_for_car_photo, reply_markup=markup)
            else:
                bot.send_message(user.id, user.replicas.wrong_transmission)
        elif user.state == 89:
            if message.photo or (message.document and message.document.thumb):
                if message.photo:
                    user.creating_race_car.photo_id = message.photo[2].file_id
                elif message.document and message.document.thumb:
                    user.creating_race_car.photo_id = message.document.file_id
                user.creating_race_car.created = True
                user.creating_race_car.save()
                to_participant_page(user)
            else:
                bot.send_message(user.id, user.replicas.wrong_type)

        user.creating_race_car.save()
