import telebot
from telebot import types

from bot import bot
from bot.controllers.payeer import send_money
from bot.controllers.user import change_language
from bot.models import User
from bot.utils.to_page_actions import to_participant_page, to_main_page, ask_for_language, to_profile_page, \
    to_referee_page, to_login_page, to_town_page, \
    to_org_page, to_balance_page, to_taxi_driver_page, to_choose_country, to_choose_continent, to_select_gender, \
    to_select_age
from bot.utils.bot_utils import process_district_input, unknown_action, process_country_input, process_continent_input
from messages import messages as mes


def profile_page_action(user: User, message: telebot.types.Message):
    if user.state == 10:
        choose_profile_action(user, message)
    elif user.state == 11:
        change_language_action(user, message)
    elif user.state == 12:
        choose_type_action(user, message)
    elif user.state == 13:
        login_page_action(user, message)
    elif user.state == 14:
        process_input_login(user, message)
    elif 15 <= user.state <= 19:
        town_action(user, message)
    # elif user.state == 19:
    #     about_page_action(user, message)
    elif user.state == 130:
        if message.text == user.replicas.male_option_text or message.text == user.replicas.female_option_text:
            user.gender = message.text
            to_profile_page(user, user.replicas.success)
        elif message.text == user.replicas.back_text:
            to_profile_page(user)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)
        else:
            bot.send_message(user.id, user.replicas.wrong_type)
    elif user.state == 131:
        if message.text == user.replicas.back_text:
            to_profile_page(user)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)
        else:
            try:
                age = int(message.text)
                user.age = age
                to_profile_page(user, user.replicas.success)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_integer_input)


def choose_profile_action(user: User, message: telebot.types.Message):
    if message.text == mes[user.lang].change_language_button:
        user.state = 11
        ask_for_language(user)
    elif message.text == mes[user.lang].my_login_button:
        to_login_page(user)
    elif message.text == mes[user.lang].my_town_button:
        to_town_page_action(user)
    elif message.text == user.replicas.my_balance_button:
        to_balance_page(user)
    elif message.text == mes[user.lang].back_text:
        to_main_page(user)
    elif message.text == user.replicas.choose_type_taxi_driver:
        to_taxi_driver_page(user, offer_car=False, select_button=True, messages_list=[user.replicas.taxi_car_page_description])
    elif message.text == user.replicas.my_login_button:
        to_login_page(user)
    elif message.text == mes[user.lang].choose_type_participant:
        to_participant_page(user, user.replicas.participant_page_description)
    elif message.text == mes[user.lang].choose_type_referee:
        to_referee_page(user, mes[user.lang].referee_page_description)
    elif message.text == user.replicas.gender_button_text:
        to_select_gender(user)
    elif message.text == user.replicas.age_button_text:
        to_select_age(user)
    else:
        unknown_action(user)


def change_language_action(user: User, message: telebot.types.Message):
    status = change_language(user, message.text)
    if status:
        to_main_page(user)
    elif message.text == mes[user.lang].back_text:
        to_profile_page(user)


def choose_type_action(user: User, message: telebot.types.Message):
    if message.text == mes[user.lang].choose_type_participant:
        to_participant_page(user, user.replicas.participant_page_description)
    elif message.text == mes[user.lang].choose_type_referee:
        to_referee_page(user, mes[user.lang].referee_page_description)
    elif message.text == mes[user.lang].choose_type_org:
        to_org_page(user, mes[user.lang].org_page_description)
    elif message.text == user.replicas.back_text:
        to_profile_page(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)


def login_page_action(user: User, message: telebot.types.Message):
    if message.text == mes[user.lang].change_login_button:
        user.state = 14
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(types.KeyboardButton(mes[user.lang].back_text))
        bot.send_message(user.id, mes[user.lang].ask_for_login, reply_markup=markup)
    elif message.text == mes[user.lang].back_text:
        to_profile_page(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
    else:
        unknown_action(user)


def process_input_login(user: User, message: telebot.types.Message):
    if message.text != '':
        if message.text == mes[user.lang].back_text:
            to_profile_page(user)
        else:
            exist = User.objects.filter(login=message.text).exists()
            if exist:
                bot.send_message(user.id, mes[user.lang].used_login)
            else:
                user.login = message.text
                to_profile_page(user, message=mes[user.lang].login_changed)


def town_page_action(user: User, message: telebot.types.Message):
    if message.text == mes[user.lang].change_town_button:
        to_choose_continent(user, state=16)
    elif message.text == mes[user.lang].back_text:
        to_profile_page(user)
    elif message.text == mes[user.lang].to_main_menu_button_text:
        to_main_page(user)
    else:
        unknown_action(user)


def to_town_page_action(user: User):
    if user.town and user.district:
        to_town_page(user)
    else:
        to_choose_continent(user, state=16)


def town_action(user: User, message: telebot.types.Message):
    if user.state == 15:
        town_page_action(user, message)
    elif user.state == 16:
        process_continent_input(user, message, lambda u: to_town_page(u) if u.district else to_profile_page(u), next_state=17)
    elif user.state == 17:
        process_country_input(user, message, lambda u: to_town_page(u) if u.district else to_profile_page(u), next_state=18)
    elif user.state == 18:
        process_district_input(user, message, lambda u: to_town_page(u) if u.district else to_profile_page(u), district_action, next_state=19)
    elif user.state == 19:
        if message.text == mes[user.lang].back_text:
            if user.district:
                to_profile_page(user)
            else:
                to_choose_continent(user, state=16)
        else:
            user.town = message.text
            # bot.send_message(user.id, user.replicas.town_updated)
            to_profile_page(user, user.replicas.town_updated)


def balance_page_action(user: User, message: telebot.types.Message):
    if user.state == 90:
        if message.text == user.replicas.add_money_button_text:
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton(user.replicas.go_to_site, url='https://payeer.com/ru/'))
            bot.send_message(user.id, user.replicas.add_money_instructions % user.id, reply_markup=markup)
        elif message.text == user.replicas.get_money_button_text:
            user.state = 91
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            markup.add(types.KeyboardButton(user.replicas.back_text),
                       types.KeyboardButton(user.replicas.to_main_menu_button_text))
            bot.send_message(user.id, user.replicas.get_money_instructions, reply_markup=markup)
        elif message.text == mes[user.lang].back_text:
            to_profile_page(user)
        elif message.text == mes[user.lang].to_main_menu_button_text:
            to_main_page(user)
    elif user.state == 91:
        if message.text == user.replicas.back_text:
            to_balance_page(user)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)
        else:
            amount, location = message.text.split(' ', 1)
            amount = float(amount)
            status = send_money(location, float(amount))
            if status:
                user.balance -= amount
                to_main_page(user, user.replicas.money_sent)
            else:
                to_main_page(user, user.replicas.not_sent)


def district_action(user, district):
    user.district = district
