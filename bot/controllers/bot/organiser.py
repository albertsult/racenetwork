import telebot

from bot import bot
from bot.models import User
from bot.utils.to_page_actions import to_profile_page, to_main_page


def org_page_action(user: User, message: telebot.types.Message):
    if user.state == 70:
        org_main_page(user, message)


def org_main_page(user: User, message: telebot.types.Message):
    if message.text == user.replicas.stat_button_text:
        answer = user.replicas.org_stat_format % (user.organiser.event_set.count(), user.organiser.rating)
        bot.send_message(user.id, answer)
    elif message.text == user.replicas.back_text:
        to_profile_page(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
