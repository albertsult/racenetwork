import logging

from telebot import types

from bot import bot
from bot.controllers.event import select_referee, vote_for_referee
from bot.models import Event, RegisteredReferee, User
from bot.services import get_responses_list
from bot.utils.to_page_actions import send_response_with_pagination, to_events_page, to_main_page, \
    to_details_response_view, send_referee_voting
from bot.utils.utils import choose_elem

bot_logger = logging.getLogger('bot_logic')


def response_callback(user: User, message, data):
    topic, data = data.split('.', 1)
    if topic == 'details':
        event = Event.objects.get(id=data)
        to_details_response_view(user, event)
    elif topic == 'accept_referee':
        event = Event.objects.get(id=data)
        if not (event.finished or event.referee):
            role = event.get_role(user)
            if role == user.replicas.choose_type_org or role == user.replicas.choose_type_participant:
                registered_referees = event.registeredreferee_set.all()
                if registered_referees.count() != 0:

                    for reg_ref in registered_referees:
                        send_referee_voting(user, reg_ref, role)
                else:
                    bot.send_message(user.id, user.replicas.no_referees)
    elif topic == 'elect':
        event = Event.objects.get(id=data)
        markup = types.InlineKeyboardMarkup()
        referees = event.registeredreferee_set.all()
        if referees:
            for referee in referees:
                markup.add(types.InlineKeyboardButton('%s' % referee.referee.user.login,
                                                      callback_data='responses.elect_referee.%d.%d' % (
                                                          event.id, referee.id)))
                bot.edit_message_text(user.replicas.elect_referee, user.id, message.message_id, reply_markup=markup)
        else:
            bot.edit_message_text(user.replicas.no_referees, user.id, message.message_id, reply_markup=markup)

    elif topic == 'elect_referee':
        event_id, referee_id = data.split('.', 1)
        event = Event.objects.get(id=event_id)
        registered_referee = RegisteredReferee.objects.get(id=referee_id)
        if not (event.finished or event.referee):
            role = event.get_role(user)
            if role == user.replicas.choose_type_participant:
                participation = event.participantinevent_set.filter(participant__user_id=user.id).first()
                if participation:
                    if not participation.voted:
                        vote_for_referee(participation, event, registered_referee)
                        bot.edit_message_text(user.replicas.success, user.id, message.message_id)
                    else:
                        bot.edit_message_text(user.replicas.voted_already, user.id, message.message_id)
                else:
                    bot_logger.warning("Non participant have election buttons")
                    bot.delete_message(user.id, message.message_id)
            else:
                bot.edit_message_text(user.replicas.referee_election_only_participants_allowed, user.id, message.message_id)
        else:
            bot.edit_message_text(user.replicas.referee_election_not_relevant, user.id, message.message_id)

    elif topic == 'discard_referee':
        bot.edit_message_text(user.replicas.success, user.id, message.message_id)

    elif topic == 'leave':
        event = Event.objects.get(id=data)
        availability = event.can_leave(user)
        if availability.status:
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton(user.replicas.confirm,
                                              callback_data='responses.confirm_leaving.%d' % event.id))
            bot.send_message(user.id, user.replicas.confirm_leaving, reply_markup=markup)
            bot.delete_message(user.id, message.message_id)
        else:
            text = ''
            for reason in availability.messages:
                text += reason + '\n'
            bot.send_message(user.id, text)
            bot.delete_message(user.id, message.message_id)

    elif topic == 'confirm_leaving':
        event = Event.objects.get(id=data)
        availability = event.leave(user)
        if availability.status:

            bot.edit_message_text(user.replicas.success, user.id, message.message_id)
        else:
            text = ''
            for reason in availability.messages:
                text += reason + '\n'
            bot.edit_message_text(text, user.id, message.message_id)

    elif topic == 'arrived':
        event = Event.objects.get(id=data)
        bot.send_message(user.id, user.replicas.participant_arrived_text % event.referee.user.phone)


def responses_page_action(user: User, message):
    if user.state == 26:
        response_list_action(user, message)
    # elif user.state == 27:
    #     responses_participants_page_action(user, message)


def response_list_action(user: User, message):
    if message.text == user.replicas.prev_response or message.text == user.replicas.next_response:
        action = 'next' if message.text == user.replicas.next_response else 'prev'
        event = choose_elem(action, user.current_response_in_list.id, get_responses_list(user))
        send_response_with_pagination(user, event)
    elif message.text == user.replicas.back_text:
        to_events_page(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
