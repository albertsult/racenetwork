import datetime
import io

import requests
import telebot
from PIL import Image
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from telebot import types

from bot import bot
from bot.controllers.bot.response import responses_page_action
from bot.controllers.event import get_top_winners, skipped_event_all_voted_check
from bot.controllers.payeer import send_money
from bot.models import User, Event, RegisteredReferee, RaceCar, ParticipantInEvent, TaxiOrder, Region
from bot.services import get_list_of_available_events
from bot.utils.bot_utils import list_of_available_events, process_district_input, \
    unknown_action, send_possible_cars_for_participation, process_continent_input, process_country_input
from bot.utils.to_page_actions import to_balance_page, to_events_page, to_main_page, \
    to_create_race_car_page, to_choose_date, to_responses_page, to_create_event_page, to_best_winners_page, \
    to_participant_list_page, to_find_event_list, to_participate_page, to_town_input_page, \
    to_choose_region, ask_for_phone, to_choose_continent, send_referee_voting
from bot.utils.utils import choose_elem
from config import payeer_income_account, token, telegram_file_link
from race_bot import settings


def find_event_callback(user: User, message: telebot.types.Message, data):
    topic, data = data.split('.', 1)
    try:
        event = Event.objects.get(id=data)
    except ValueError:
        event_id, data = data.split('.', 1)
        event = Event.objects.get(id=event_id)

    if event.time < timezone.now() or (
            event.max_participants == ParticipantInEvent.objects.filter(event=event).count() and event.referee):
        bot.edit_message_text(user.replicas.invalid_event, user.id, message.message_id)
    elif topic == 'details':
        event = Event.objects.get(id=data)
        answer = event.format(user.replicas, spec='find')
        markup = types.InlineKeyboardMarkup()
        markup.add(
            types.InlineKeyboardButton(user.replicas.participate, callback_data='find.participate.%d' % event.id))
        bot.edit_message_text(answer, user.id, message.message_id, reply_markup=markup)
    elif topic == 'participate':
        if event.creator:
            participate_in_poster(user, event, message=message)
        else:
            if event.can_register_as_referee(user):
                markup = types.InlineKeyboardMarkup()
                markup.add(
                    types.InlineKeyboardButton(user.replicas.participate_as_referee,
                                               callback_data='find.referee.%d' % event.id),
                    types.InlineKeyboardButton(user.replicas.participate_as_player,
                                               callback_data='find.participant.%d' % event.id),
                )
                bot.edit_message_reply_markup(user.id, message.message_id, reply_markup=markup)
            else:
                bot.edit_message_text(user.replicas.cant_participate_in_event, user.id, message.message_id)
    elif topic == 'referee':
        event = Event.objects.get(id=data)
        if event.can_register_as_referee(user):
            if user.phone:
                markup = types.InlineKeyboardMarkup()
                markup.add(
                    types.InlineKeyboardButton(user.replicas.confirm,
                                               callback_data='find.confirm_referee.%d' % event.id))
                referee_requirement = ''
                if event.race_type == 1:
                    referee_requirement = user.replicas.drug_race_referee_requirement
                elif event.race_type == 2:
                    referee_requirement = user.replicas.street_race_referee_requirement
                elif event.race_type == 3:
                    referee_requirement = user.replicas.trophy_race_referee_requirement
                bot.edit_message_text(
                    user.replicas.referee_role_description % event.input_sum + '\n\n%s' % referee_requirement,
                    user.id, message.message_id, reply_markup=markup)
            else:
                ask_for_phone(user)

    elif topic == 'participant':
        event = Event.objects.get(id=data)
        send_possible_cars_for_participation(user, event, user.replicas.select_car, message)
    elif topic == 'confirm_referee':
        event = Event.objects.get(id=data)
        if user.balance > event.input_sum:
            if user.phone:
                if event.register_as_referee(user):
                    bot.edit_message_text(user.replicas.success, user.id, message.message_id)
                    to_events_page(user, '✔️')

                else:
                    bot.edit_message_text(user.replicas.cant_register_as_referee, user.id, message.message_id)
            else:
                ask_for_phone(user)
        else:
            bot.edit_message_text(user.replicas.not_enough_money, user.id, message.message_id)
            to_balance_page(user)
    elif topic == 'participate_with_car':
        car = RaceCar.objects.get(id=data)
        status = event.can_participate(car)
        if status.status:
            markup = types.InlineKeyboardMarkup()
            markup.add(
                types.InlineKeyboardButton(user.replicas.confirm,
                                           callback_data='find.participate_with_car_confirm.%d.%d' % (
                                           event.id, car.id)))
            bot.edit_message_text(user.replicas.participant_role_description % event.input_sum, user.id,
                                  message.message_id, reply_markup=markup)
        else:
            reasons_text = ''
            for reason in status.reasons:
                reasons_text += reason + '\n'
            send_possible_cars_for_participation(user, event, reasons_text, message)
    elif topic == 'participate_with_car_confirm':
        car = RaceCar.objects.get(id=data)
        result = event.participate(user, car)
        if result.status:
            bot.edit_message_text(user.replicas.success, user.id, message.message_id)
            to_events_page(user, '✔️')
        else:
            if user.replicas.not_enough_money in result.messages:
                bot.edit_message_text(user.replicas.not_enough_money, user.id, message.message_id)
                to_balance_page(user)
            else:
                text = ''
                for m in result.messages:
                    text += m + '\n'

                bot.edit_message_text(text, user.id, message.message_id)


def rate_callback(user: User, message: telebot.types.Message, data):
    data = data.split('.')
    role = data[0]
    rate = int(data[1])

    if role == 'participant':
        participant = ParticipantInEvent.objects.get(id=data[3])
        compute_new_rating(participant.participant, rate)
    elif role == 'referee':
        event = Event.objects.get(id=data[2])
        if event.skipped:
            if timezone.now() - datetime.timedelta(days=2) < event.time:
                participant = event.participantinevent_set.get(participant__id=user.participant.id)
                participant.voted = True
                participant.save()
                skipped_event_all_voted_check(event)
            else:
                bot.delete_message(user.id, message.message_id)
                return
        else:
            referee = event.referee
            compute_new_rating(referee, rate)
    elif role == 'org':
        event = Event.objects.get(id=data[2])
        org = event.creator
        compute_new_rating(org, rate)
    elif role == 'taxi_driver':
        order = TaxiOrder.objects.get(id=data[2])
        if not order.voted:
            compute_new_rating(order.car.driver.taxidriver, rate)
            order.voted = True
            order.save()
        else:
            bot.edit_message_text(user.replicas.voted_already, user.id, message.message_id)
            return
    bot.edit_message_text(user.replicas.success_rate, user.id, message.message_id)


def winners_callback(user: User, message: telebot.types.Message, data):
    topic, data = data.split('.', 1)

    if topic == 'top':
        category, action, offset = data.split('.', 2)
        offset = int(offset)
        if action == 'next':
            offset += 5
        else:
            offset -= 5
        participants = get_top_winners(category, offset)
        answer = ''
        for part in participants:
            answer += user.replicas.top_winner_format % (part.user.login, part.wins)
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton('<<', callback_data='winners.top.%s.prev.%d' % (category, offset)),
                   types.InlineKeyboardButton('>>', callback_data='winners.top.%s.next.%d' % (category, offset)))
        bot.edit_message_text(answer, user.id, message.message_id, reply_markup=markup)


def events_page_action(user: User, message: telebot.types.Message):
    if message.text == user.replicas.events_menu_event_search:
        to_choose_region(user, state=20)
    elif message.text == user.replicas.events_menu_responses:
        to_responses_page(user)
    elif message.text == user.replicas.events_menu_event_create:
        if user.creating_event:
            if not user.creating_event.created:
                user.creating_event.delete()
                user.creating_event = None
        to_create_event_page(user)

    elif message.text == user.replicas.best_winners:
        to_best_winners_page(user)

    elif message.text == user.replicas.back_text:
        to_main_page(user)


def find_event_action(user: User, message: telebot.types.Message):
    if user.state == 20:
        if message.text == user.replicas.back_text:
            to_events_page(user)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)
        else:
            try:
                region = Region.get_by_name(message.text)
                list_of_available_events(user, region)
            except ObjectDoesNotExist:
                unknown_action(user)
            # to_events_page(user)


def event_action(user: User, message: telebot.types.Message):
    if 20 <= user.state < 22:
        find_event_action(user, message)
    elif 22 <= user.state < 23:
        top_winners_action(user, message)
    elif 23 <= user.state < 26:
        find_event_list_action(user, message)
    elif 26 <= user.state < 27:
        responses_page_action(user, message)
    elif 30 <= user.state < 50:
        create_event_action(user, message)


def compute_new_rating(involved, new_vote):
    old_rating = involved.rating
    old_voted = involved.voted_times
    involved.rating = (old_rating * old_voted + new_vote) / (old_voted + 1)
    involved.voted_times += 1
    involved.save()


def create_event_action(user: User, message: telebot.types.Message):
    if message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
    elif user.state == 30:
        if message.text == user.replicas.set_event or message.text == user.replicas.create_event_as_participant_text:
            event = Event()
            event.save()
            user.creating_event = event
            if message.text == user.replicas.create_event_as_participant_text:
                if user.instantiated():
                    if user.race_cars.filter(created=True).count() == 0:
                        bot.send_message(user.id, user.replicas.no_cars_message)
                        to_create_race_car_page(user)
                        return
                    else:
                        participant_event_creation(user, event)
                else:
                    return
            else:
                user.state = 32
                event.creator = user.organiser
                bot.send_message(user.id, user.replicas.poster_create_payment_notification)
                if user.balance >= 1000:
                    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                    markup.add(types.KeyboardButton(user.replicas.back_text),
                               types.KeyboardButton(user.replicas.to_main_menu_button_text))
                    to_choose_date(user)
                    bot.send_message(user.id, '✔️', reply_markup=markup)
                else:
                    to_balance_page(user, user.replicas.not_enough_money_to_create_poster)
        elif message.text == user.replicas.back_text:
            to_events_page(user, '✔️')
    elif user.state == 31:
        if message.text == user.replicas.add_car_button_text:
            to_create_race_car_page(user)
        elif message.text == user.replicas.back_text:
            to_events_page(user, '✔️')
    elif user.state == 32:
        if message.text == user.replicas.back_text:
            if user.creating_event.creator:
                to_events_page(user)
            else:
                participant_event_creation(user)

        else:
            try:
                date = datetime.datetime.strptime(message.text, '%d.%m.%y').date()
                if datetime.datetime.now().date() <= date < datetime.datetime.now().date() + datetime.timedelta(days=30*4):
                    user.creating_event.time = user.creating_event.time.replace(year=date.year,
                                                                                month=date.month,
                                                                                day=date.day)
                    ask_for_time(user)
                else:
                    bot.send_message(user.id, user.replicas.wrong_date_input)
            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_data_format)
    elif user.state == 33:
        if message.text == user.replicas.back_text:
            to_choose_date(user, state=32)
        else:
            try:
                time = datetime.datetime.strptime(message.text, '%H:%M').time()
                user.creating_event.time = user.creating_event.time.replace(hour=time.hour, minute=time.minute)
                to_choose_region(user, state=35, choose_continent_state=34)

            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_time_format)
    elif user.state == 34:
        process_continent_input(user, message, ask_for_time, next_state=34.5)
    elif user.state == 34.5:
        process_country_input(user, message, ask_for_time, next_state=35)
    elif user.state == 35:
        process_district_input(user, message, ask_for_time, district_event_action,
                               message_to_send=user.replicas.ask_for_town_event)
    elif user.state == 36:
        if message.text == user.replicas.back_text:
            ask_for_time(user)
        else:
            user.creating_event.town = message.text
            if user.creating_event.creator:
                ask_for_event_photo(user)
            else:
                ask_for_max_participants(user)
    elif user.state == 37:
        if message.text == user.replicas.back_text:
            to_town_input_page(user, need_to_main_menu=True, next_state=36, message=user.replicas.ask_for_town_event)
        else:
            try:
                max_participants = int(message.text)
                if 1 < max_participants <= 10:
                    user.creating_event.max_participants = max_participants
                    ask_for_race_type(user)
                else:
                    bot.send_message(user.id, user.replicas.wrong_max_participants_number)

            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_max_participants)

    elif user.state == 38:
        if message.text == user.replicas.back_text:
            ask_for_max_participants(user)
        else:
            if message.text == user.replicas.drug_race_text \
                    or message.text == user.replicas.street_race_text \
                    or message.text == user.replicas.trophy_race_text \
                    or message.text == user.replicas.tag_of_war_race_text:
                if message.text == user.replicas.drug_race_text:
                    user.creating_event.race_type = 1
                elif message.text == user.replicas.street_race_text:
                    user.creating_event.race_type = 2
                elif message.text == user.replicas.trophy_race_text:
                    user.creating_event.race_type = 3
                elif message.text == user.replicas.tag_of_war_race_text:
                    user.creating_event.race_type = 4
                ask_for_event_photo(user)
            else:
                bot.send_message(user.id, user.replicas.wrong_type)

    elif user.state == 40:
        if message.text == user.replicas.back_text:
            if user.creating_event.creator:
                to_town_input_page(user, need_to_main_menu=True, next_state=36,
                                   message=user.replicas.ask_for_town_event)
            else:
                ask_for_race_type(user)
        else:
            if message.photo or (message.document and message.document.thumb):
                if message.photo:
                    user.creating_event.photo_id = message.photo[-1].file_id
                elif message.document and message.document.thumb:
                    user.creating_event.photo_id = message.document.file_id
                if user.creating_event.creator:
                    if user.balance >= 1000:
                        create_event(user)
                    else:
                        to_balance_page(user, user.replicas.not_enough_money_to_create_poster)
                else:
                    user.state = 41
                    bot.send_message(user.id, user.replicas.ask_for_participant_sum)
            else:
                bot.send_message(user.id, user.replicas.wrong_type)
    elif user.state == 41:
        if message.text == user.replicas.back_text:
            ask_for_event_photo(user)
        else:
            try:
                input_sum = float(message.text)
                if input_sum >= 1000:
                    user.creating_event.input_sum = int(message.text)
                    if ParticipantInEvent.objects.filter(event=user.creating_event,
                                                         participant=user.participant).first():
                        if user.balance > user.creating_event.input_sum:
                            create_event(user)
                        else:
                            bot.send_message(user.id, user.replicas.not_enough_money)
                            to_balance_page(user)
                            return
                else:
                    bot.send_message(user.id, user.replicas.wrong_input_sum_participant)

            except ValueError:
                bot.send_message(user.id, user.replicas.wrong_input_participant)

    user.creating_event.save()


def delete_and_to_main(user):
    user.creating_event.delete()
    user.creating_event = None
    to_main_page(user)


def district_event_action(user, district):
    user.creating_event.district = district
    user.state = 36
    user.save()


def create_event(user):
    event = user.creating_event
    status = True
    if event.time.date() == timezone.now().date():
        event.referee = user.referee
    elif event.time.date() < timezone.now().date():
        status = False
        bot.send_message(user.id, user.replicas.wrong_date_input)
    if status:
        user.creating_event.created = True
        user.creating_event.save()
        if user.creating_event.creator:
            user.balance -= 1000
            send_money(payeer_income_account, 1000)
            to_main_page(user, user.replicas.poster_created)
        else:
            user.balance -= user.creating_event.input_sum
            bot.send_message(user.id, user.replicas.withdraw_inform)
            to_main_page(user, user.replicas.event_created)
        users = User.objects.filter(district=user.creating_event.district).distinct().all()
        if not settings.DEBUG:
            users = users.exclude(id=user.id)
        for u in users:
            if event.photo_id:
                try:
                    url = telegram_file_link % (token, bot.get_file(event.photo_id).file_path)
                    r = requests.get(url)
                    bot.send_photo(u.id, photo=r.content)
                except Exception as e:
                    print(e)
                    pass
            try:
                bot.send_message(u.id, u.replicas.new_event_created)
            except Exception as e:
                print(e)
                pass


def top_winners_action(user, message):
    if message.text != user.replicas.back_text:
        if message.text == user.replicas.drug_race_text \
                or message.text == user.replicas.street_race_text \
                or message.text == user.replicas.trophy_race_text \
                or message.text == user.replicas.tag_of_war_race_text:
            if message.text == user.replicas.drug_race_text:
                race_type = 1
            elif message.text == user.replicas.street_race_text:
                race_type = 2
            elif message.text == user.replicas.trophy_race_text:
                race_type = 3
            else:
                race_type = 4
            participants = get_top_winners(race_type)
            answer = ''
            for part in participants:
                answer += user.replicas.top_winner_format % (part.user.login, part.wins)
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton('<<', callback_data='winners.top.%d.prev.0' % race_type),
                       types.InlineKeyboardButton('>>', callback_data='winners.top.%d.next.0' % race_type))
            bot.send_message(user.id, '🏆🥇🥈🥉🏆')
            bot.send_message(user.id, answer, reply_markup=markup)

            # to_events_page(user, '✔️')
        else:
            bot.send_message(user.id, user.replicas.wrong_data_format)
    else:
        to_events_page(user)


def find_event_list_action(user: User, message):
    if user.state == 23:
        if message.text == user.replicas.next_event or message.text == user.replicas.prev_event:
            action = 'next' if message.text == user.replicas.next_event else 'prev'
            events = get_list_of_available_events(user, user.find_event_district)
            new_event = choose_elem(action, user.find_current_event.id, events)
            user.find_current_event = new_event
            to_find_event_list(user, new_event)
        elif message.text == user.replicas.pilots_button:
            if user.find_current_event.creator:
                bot.send_message(user.id, user.replicas.poster_event_pilots_information)
            else:
                participant = user.find_current_event.participantinevent_set.first()
                to_participant_list_page(user, user.find_current_event, participant, edit_event=True)
        elif message.text == user.replicas.participate:
            if user.instantiated():
                if user.find_current_event.can_register_as_referee(user):
                    if user.find_current_event.creator:
                        participate_in_poster(user, user.find_current_event)
                    else:
                        to_participate_page(user)
                else:
                    bot.edit_message_text(user.replicas.cant_participate_in_event, user.id, message.message_id)
        elif message.text == user.replicas.back_text:
            to_events_page(user)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)
    elif user.state == 24:
        if message.text == user.replicas.next_pilot or message.text == user.replicas.prev_pilot:
            action = 'next' if message.text == user.replicas.next_pilot else 'prev'
            participants = user.find_current_event.participantinevent_set.all()
            new_participant = choose_elem(action, user.find_event_current_participant.id, participants)
            user.find_event_current_participant = new_participant
            to_participant_list_page(user, user.find_current_event, new_participant)
        elif message.text == user.replicas.participate:
            if user.find_current_event.creator:
                participate_in_poster(user, user.find_current_event)
            else:
                to_participate_page(user)

        elif message.text == user.replicas.back_text:
            to_find_event_list(user, user.find_current_event)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)
    elif user.state == 25:
        if message.text == user.replicas.choose_type_participant:
            event = user.find_current_event
            send_possible_cars_for_participation(user, event, user.replicas.select_car)
        elif message.text == user.replicas.choose_type_referee:
            event = user.find_current_event
            if not user.referee.id in list(map(lambda reg_ref: reg_ref.referee.id, event.registeredreferee_set.all())):
                if user.phone:
                    markup = types.InlineKeyboardMarkup()
                    markup.add(
                        types.InlineKeyboardButton(user.replicas.confirm,
                                                   callback_data='find.confirm_referee.%d' % event.id))
                    referee_requirement = ''
                    if event.race_type == 1:
                        referee_requirement = user.replicas.drug_race_referee_requirement
                    elif event.race_type == 2:
                        referee_requirement = user.replicas.street_race_referee_requirement
                    elif event.race_type == 3:
                        referee_requirement = user.replicas.trophy_race_referee_requirement
                    bot.send_message(
                        user.id,
                        user.replicas.referee_role_description % event.input_sum + '\n\n%s' % referee_requirement,
                        reply_markup=markup)
                else:
                    ask_for_phone(user)
        elif message.text == user.replicas.back_text:
            to_events_page(user)
        elif message.text == user.replicas.to_main_menu_button_text:
            to_main_page(user)


def ask_for_race_type(user):
    user.state = 38
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    markup.add(types.KeyboardButton(user.replicas.drug_race_text),
               types.KeyboardButton(user.replicas.street_race_text),
               types.KeyboardButton(user.replicas.trophy_race_text),
               types.KeyboardButton(user.replicas.tag_of_war_race_text),
               types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))

    bot.send_message(user.id, user.replicas.ask_for_type, reply_markup=markup)


def participate_in_poster(user, event, message=None):
    participation = user.participant.participantinevent_set.filter(event=event).first()
    if not participation:
        participation = ParticipantInEvent(participant=user.participant, event=event)
        participation.save()
        if message:
            bot.edit_message_text(user.replicas.participate_now, user.id, message.message_id)
        else:
            bot.send_message(user.id, user.replicas.participate_now)
    else:
        if message:
            bot.edit_message_text(user.replicas.participate_already, user.id, message.message_id)
        else:
            bot.send_message(user.id, user.replicas.participate_already)


def ask_for_event_photo(user):
    user.state = 40
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, user.replicas.ask_for_event_photo, reply_markup=markup)


def ask_for_time(user):
    user.state = 33
    bot.send_message(user.id, user.replicas.ask_for_time)


def ask_for_max_participants(user):
    user.state = 37
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, user.replicas.ask_for_max_participants, reply_markup=markup)


def participant_event_creation(user, event=None):
    user.state = 31
    if event:
        participation = ParticipantInEvent(event=event, participant=user.participant)
        participation.save()
    cars = user.race_cars.filter(created=True).all()
    for car in cars:
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(user.replicas.select,
                                              callback_data='create_event.car.%d' % car.id))
        bot.send_message(user.id, car.format(user.replicas), reply_markup=markup)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(
        types.KeyboardButton(user.replicas.back_text),
        types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, '✔️', reply_markup=markup)
