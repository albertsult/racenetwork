import datetime
import json
import logging

import requests
from django.utils.timezone import pytz, now as timezone_now

from bot.controllers.payment import proceed_payment_comment
from bot.models import Time
from config import payeer_api_pass, payeer_api_id, payeer_account
from race_bot.settings import TIME_ZONE

logger = logging.getLogger("payeer_controller")

tz = pytz.timezone(TIME_ZONE)


def get_json_data(begin, end):
    for i in range(0, 10):
        try:
            params = {
                'account': payeer_account,
                'apiId': payeer_api_id,
                'apiPass': payeer_api_pass,
                'action': 'history',
                'type': 'incoming',
                'from': begin.strftime('%Y-%m-%d %H:%M:%S'),
                'to': end.strftime('%Y-%m-%d %H:%M:%S'),
            }
            # headers = {
            #     'Content-Type': 'application/json'
            # }
            r = requests.post('https://payeer.com/ajax/api/api.php',
                              data=params)
            data = r.text
            data = json.loads(data)
            return data
        except Exception:
            logger.critical("Can't parse json")


def send_money(payeer_id, amount):
    params = {
        'account': payeer_account,
        'apiId': payeer_api_id,
        'apiPass': payeer_api_pass,
        'action': 'transfer',
        'curIn': 'RUB',
        'curOut': 'RUB',
        'sum': amount,
        'to': str(payeer_id),
    }
    r = requests.post('https://payeer.com/ajax/api/api.php',
                      data=params)
    data = r.text
    data = json.loads(data)
    if 'errors' in data and data['errors']:
        ret = not data['errors']
        logging.critical(data)
    else:
        if 'historyId' in data:
            ret = int(data['historyId']) > 0
        else:
            ret = False
    return ret


def run():
    try:
        now = timezone_now().astimezone(tz)
        check_time = Time.objects.first()
        if not check_time:
            check_time = Time(time=now)
            previous_check = now - datetime.timedelta(days=10)
        else:
            previous_check = check_time.time.astimezone(tz)
        data = get_json_data(previous_check, now)
        errors = int(data['auth_error'])
        if errors != 0:
            raise Exception(data['errors'])
        else:
            history = data['history']
            if len(history):
                for key, operation in history.items():
                    try:
                        if operation['to'] == payeer_account and operation['status'] == 'success':
                            comment = operation['comment']
                            amount = float(operation['creditedAmount'])
                            proceed_payment_comment(comment, amount)
                    except Exception as e:
                        logger.warning('Unhandled payment\n' + str(e))
            check_time.time = now
            check_time.save()
    except Exception as e:
        import traceback
        traceback.print_exc()
        logger.error(e.__repr__())