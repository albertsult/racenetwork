from bot.models import User, Referee, Participant, Organiser, TaxiDriver
from bot.utils.to_page_actions import ask_for_language
from messages import messages


def get_user(user, ref_id=None):

    result = User.objects.filter(id=user.id).first()
    if result is None:
        result = _create_user(user, ref_id)
        return False, result
    else:
        return True, result


def _create_user(telegram_user, ref_id):
    ref_user = User.objects.filter(id=ref_id).first()
    user = User(id=telegram_user.id)
    if ref_user:
        user.ref_user = ref_user
    user.save()
    referee = Referee(user=user)
    participant = Participant(user=user)
    organiser = Organiser(user=user)
    taxi_driver = TaxiDriver(user=user)
    new_user_action(user)
    referee.save()
    participant.save()
    organiser.save()
    taxi_driver.save()
    return user


def new_user_action(user):
    ask_for_language(user, first=True)


def change_language(user, message):
    if message in messages:
        user.lang = message
        return True
    return False
