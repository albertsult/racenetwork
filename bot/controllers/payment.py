from telebot import types

from bot import bot
from bot.models import User


def payment_callback(user: User, message: types.Message, data):
    if data == 'get_id':
        bot.send_message(user.id, '`%s`' % user.id, parse_mode='Markdown')


def proceed_payment_comment(comment, amount):
    user = User.objects.filter(id=comment).first()
    if user:
        user.balance += amount
        user.save()
        if user.ref_user:
            user.ref_user.balance += amount * 0.01  # Problems with rounding and other float points problem have not been tested and looked up
            user.ref_user.save()
        bot.send_message(user.id, user.replicas.success_payment % amount)

