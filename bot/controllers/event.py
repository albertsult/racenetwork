import datetime

from django.db.models import F, Count, Case, When, Q
from django.utils import timezone

from bot import bot
from bot.models import Event, Participant, ParticipantInEvent, RegisteredReferee
from bot.utils.bot_utils import ask_to_rate_involved, ask_to_rate
from bot.utils.utils import local_now, month_from_now
from interest_rates import not_org_winner_income, not_org_referee_income


def vote_for_referee(participant: ParticipantInEvent, event: Event, registered_referee: RegisteredReferee):
    if participant.voted:
        raise ValueError("Participant have not been voted before")
    if registered_referee not in event.registeredreferee_set.all():
        raise ValueError("Registered referee have to be in this event")
    registered_referee.votes += 1
    registered_referee.save()
    participant.voted = True
    participant.save()
    if event.participantinevent_set.filter(voted=True).count() == event.max_participants:
        select_referee(event)


def end_of_event(event, winner):

    event.winner = winner
    event.finished = True
    event.save()

    registered_referees = event.registeredreferee_set.all()
    return_registered_referee_fees(registered_referees, event)

    participants_count = event.participantinevent_set.count()
    total_sum = participants_count * event.input_sum

    winner_sum = total_sum * not_org_winner_income / 100
    user = winner.user
    user.balance += winner_sum
    user.save()
    bot.send_message(winner.user.id, winner.user.replicas.winner_get_sum % (event.id, winner_sum))

    referee_sum = total_sum * not_org_referee_income / 100
    user = event.referee.user
    user.balance += referee_sum
    user.save()
    bot.send_message(event.referee.user.id, event.referee.user.replicas.referee_get_sum % (event.id, referee_sum))

    ask_to_rate_involved(event)


def elect_referees():
    events = Event.objects.filter(finished=False, creator=None, referee=None, created=True, time__lt=local_now() + datetime.timedelta(days=2)).all()
    for event in events:
        select_referee(event)


def notify_referees():
    events = Event.objects.filter(time__lt=local_now(), notified=False)
    for event in events:
        event.notified = True
        event.save()
        if event.referee:
            user = event.referee.user
            bot.send_message(user.id, user.replicas.event_date_notification % event.id)


def select_referee(event: Event):
    referees = event.registeredreferee_set.all()
    if event.participantinevent_set.count() > 1:
        if referees.count() != 0:
            selected_referee = referees[0]
            max_votes = -1
            for referee in referees:
                if referee.votes > max_votes:
                    selected_referee = referee
                    max_votes = referee.votes
            event.set_up_referee(selected_referee)
            referees = referees.exclude(id=selected_referee.id)
            for referee in referees:
                user = referee.referee.user
                bot.send_message(user.id, user.replicas.referee_auto_elected % event.format(user.replicas, spec='full'))
        else:
            participants = event.participantinevent_set.all()
            for participant in participants:
                user = participant.participant.user
                user.balance += event.input_sum
                user.save()
                bot.send_message(user.id, user.replicas.no_referees_found % event.involved_format(
                    user, event.get_role(user)))
            event.delete()
    else:
        for participant in event.participantinevent_set.all():

            user = participant.participant.user
            user.balance += event.input_sum
            user.save()
            bot.send_message(user.id, user.replicas.event_canceled_one_participant_reason_text % event.involved_format(
                    user, event.get_role(user)))

        for reg_ref in event.registeredreferee_set.all():
            user = reg_ref.referee.user
            user.balance += event.input_sum
            user.save()
            bot.send_message(user.id, user.replicas.event_canceled_one_participant_reason_text % event.involved_format(
                user, event.get_role(user)))
        event.delete()

def get_top_winners(category, offset=0):
    participants = Participant.objects.all().\
        annotate(wins=Count(
            Case(
                When(Q(participantinevent__event__winner__id=F('id'))
                     & Q(participantinevent__event__race_type=category)
                     & Q(participantinevent__event__time__gt=month_from_now()), then=1)))).order_by('-wins')[offset:offset+5]
    return participants


def skip_selecting_winner():
    events = Event.objects\
        .filter(time__lte=timezone.now() - datetime.timedelta(days=1),
                created=True,
                winner=None,
                finished=False,
                skipped=False,
                creator=None,
                )\
        .exclude(referee=None).distinct()

    for event in events:
        event.skipped = True
        participants = event.participantinevent_set.all()
        for part in participants:
            part.voted = False
            part.save()
            user = part.participant.user
            bot.send_message(user.id, user.replicas.event_skipped)
            ask_to_rate(user, event.referee, event, 'referee')
        event.save()


def skipped_event_all_voted_check(event: Event):
    if event.participantinevent_set.filter(voted=True).count() == event.participantinevent_set.count():
        event.finished = True
        event.save()
        participants = event.participantinevent_set.all()
        input_sum = event.input_sum
        penalty_sum = input_sum / participants.count()
        for part in participants:
            user = part.participant.user
            bot.send_message(user.id, user.replicas.skipped_input_sum_returned_with_referee % (event.id, input_sum, penalty_sum))
            user.balance += penalty_sum + input_sum
            user.save()

        registered_referees = event.registeredreferee_set.exclude(referee=event.referee)
        return_registered_referee_fees(registered_referees, event)


def finish_skipped_events():
    events = Event.objects.filter(skipped=True, finished=False, time__lte=timezone.now() - datetime.timedelta(days=2))
    for event in events:
        event.finished = True
        participants = event.participantinevent_set.all()
        input_sum = event.input_sum
        for part in participants:
            user = part.participant.user
            bot.send_message(user.id, user.replicas.skipped_input_sum_returned % (event.id, input_sum))
            user.balance += input_sum
            user.save()
        registered_referees = event.registeredreferee_set.exclude(referee=event.referee)
        return_registered_referee_fees(registered_referees, event)
        event.save()


def return_registered_referee_fees(registered_referees, event):
    for reg_ref in registered_referees:
        reg_ref.referee.user.balance += event.input_sum
        reg_ref.referee.user.save()
        bot.send_message(reg_ref.referee.user.id,
                         reg_ref.referee.user.replicas.user_input_sum_returned % (event.id, event.input_sum))


