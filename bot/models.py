import logging
from datetime import datetime, timedelta

import requests
from django.db import models
from django.db.models import Q
from django.utils import timezone
from telebot import types

from bot import bot
from bot.utils.utils import local_now
from config import telegram_file_link, token
from interest_rates import org_present_referee_income
from messages import messages, rus, eng

logger = logging.getLogger("models")


class Availability:

    def __init__(self, status: bool, reasons):
        self.status = status
        self.messages = reasons


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(BaseModel):
    id = models.IntegerField(primary_key=True)
    phone = models.CharField(null=True, max_length=40)

    district = models.ForeignKey('Region', null=True, on_delete=models.SET_NULL, related_name='users_own_set')
    town = models.CharField(max_length=100, null=True)
    login = models.CharField(max_length=50, null=True)
    balance = models.FloatField(default=0) # For testing purposes only
    age = models.IntegerField(null=True)
    gender = models.CharField(max_length=10, null=True)
    lang = models.CharField(max_length=15, null=True)

    state = models.FloatField(default=0)
    ref_user = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)

    find_event_district = models.ForeignKey('Region', null=True, on_delete=models.SET_NULL,
                                            related_name='find_event_set')
    find_current_event = models.ForeignKey('Event', on_delete=models.SET_NULL, null=True,
                                           related_name='find_current_event_user_set')
    find_event_current_participant = models.ForeignKey('ParticipantInEvent', null=True, on_delete=models.SET_NULL)
    current_response_in_list = models.ForeignKey('Event', on_delete=models.SET_NULL, null=True,
                                                 related_name='current_response_user_set')

    creating_race_car = models.ForeignKey('RaceCar', null=True, on_delete=models.SET_NULL)
    creating_taxi_car = models.ForeignKey('TaxiCar', null=True, on_delete=models.SET_NULL)
    creating_event = models.ForeignKey('Event', null=True, on_delete=models.SET_NULL, related_name='creating_user_set')
    creating_order = models.OneToOneField('TaxiOrder', on_delete=models.SET_NULL, null=True,
                                          related_name='user_creating_now')

    find_orders_car = models.ForeignKey('TaxiCar', null=True, on_delete=models.SET_NULL, related_name='find_orders_set')

    def __str__(self):
        return '%d %s %s' % (self.id, str(self.login), str(self.phone))
    # state_before_location = models.FloatField(null=True)
    # state_after_location = models.FloatField(null=True)

    @property
    def replicas(self):
        if self.lang:
            return messages[self.lang]
        else:
            return list(messages.values())[0]

    def instantiated(self, to_instantiate_page=True):
        status = True
        reasons = []
        if not self.gender:
            reasons.append(self.replicas.gender_is_not_instantiated)
            status = False
        if not self.age:
            reasons.append(self.replicas.age_is_not_instantiated)
            status = False
        if not self.district:
            reasons.append(self.replicas.location_is_not_instantiated)
            status = False
        if not self.phone:
            reasons.append(self.replicas.phone_is_not_instantiated)
            status = False
        if not self.login:
            reasons.append(self.replicas.login_is_not_instantiated)
            status = False

        if status:
            return True
        else:
            if to_instantiate_page:
                text = ''
                for reason in reasons:
                    text += reason + '\n'
                bot.send_message(self.id, text + '\n' + self.replicas.user_is_not_instantiated)
            return False


class TaxiCar(BaseModel):
    driver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='taxi_cars')
    price = models.FloatField(null=True)
    day_price = models.FloatField(null=True, default=0)
    color = models.CharField(max_length=25)
    brand = models.CharField(max_length=50)
    prod_year = models.IntegerField(default=2000)
    photo_id = models.CharField(max_length=100, null=True)

    created = models.BooleanField(default=False)

    def send_to_driver(self, user, buttons):
        if self.photo_id:
            try:
                url = telegram_file_link % (token, bot.get_file(self.photo_id).file_path)
                r = requests.get(url)
                bot.send_photo(user.id, photo=r.content)
            except:
                pass

        markup = types.InlineKeyboardMarkup()
        markup.add(*buttons)
        bot.send_message(user.id, self.format(user.replicas, spec='driver'), reply_markup=markup)

    def send_to_client(self, user, buttons):
        if self.photo_id:
            try:
                url = telegram_file_link % (token, bot.get_file(self.photo_id).file_path)
                r = requests.get(url)
                bot.send_photo(user.id, photo=r.content)
            except:
                pass

        markup = types.InlineKeyboardMarkup()
        markup.add(*buttons)
        bot.send_message(user.id, self.format(user.replicas, spec='client'), reply_markup=markup)

    def format(self, replicas, spec='driver'):
        if spec == 'driver':
            return replicas.taxi_car_format % (self.brand,
                                               self.color,
                                               self.price,
                                               self.day_price,
                                               self.prod_year,
                                               self.taxiorder_set.filter(finished=True,
                                                                         accepted=True).count())
        elif spec == 'client':
            driver = self.driver
            return replicas.taxi_driver_format % (driver.town,
                                                  driver.login,
                                                  driver.taxidriver.rating,
                                                  self.brand,
                                                  self.color,
                                                  self.price,
                                                  self.day_price,
                                                  self.prod_year,
                                                  self.taxiorder_set.filter(finished=True,
                                                                            accepted=True).count())

    def get_price(self, hours):
        result = 0
        while hours >= 24:
            result += self.day_price
            hours -= 24
        result += self.price * hours
        return result

    def can_deleted(self):
        orders = self.taxiorder_set.filter(accepted=True, finished=False).all()
        if orders.count() >= 0:
            return False
        else:
            return True


class RaceCar(BaseModel):
    engine_volume = models.FloatField(default=0)
    prod_year = models.IntegerField(default=2000)
    power = models.FloatField(default=0)
    drive_unit = models.IntegerField(default=0)
    brand = models.CharField(max_length=50)
    color = models.CharField(max_length=25)
    photo_id = models.CharField(max_length=100)
    transmission = models.IntegerField(default=0)

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='race_cars')
    created = models.BooleanField(default=False)

    def format(self, replicas):
        drive_unit = self.get_drive_unit_text(replicas)
        transmission = self.get_transmission_text(replicas)
        return replicas.car_format % (self.brand,
                                      self.engine_volume,
                                      self.power,
                                      self.prod_year,
                                      drive_unit,
                                      self.color,
                                      transmission)

        # elif spec == 'taxi_car_format':
        #     return replicas.taxi_car_format % (self.model, self.prod_year, self.power, price)

    def get_drive_unit_text(self, replicas):
        if self.drive_unit == 0:
            drive_unit = replicas.front_wheel
        elif self.drive_unit == 1:
            drive_unit = replicas.rear_wheel
        else:
            drive_unit = replicas.four_wheel
        return drive_unit

    def get_transmission_text(self, replicas):
        if self.transmission == 0:
            transmission = replicas.mechanical_transmission
        elif self.transmission == 1:
            transmission = replicas.auto_transmission
        else:
            logger.critical('Transmission have unexpected value %d', self.transmission)
            transmission = 'Нет'
        return transmission


class Event(BaseModel):
    time = models.DateTimeField(default=local_now)
    input_sum = models.IntegerField(null=True)
    max_participants = models.IntegerField(null=True)
    race_type = models.IntegerField(null=True)
    photo_id = models.CharField(max_length=100)

    district = models.ForeignKey('Region', on_delete=models.CASCADE, related_name='event_set', null=True)
    town = models.CharField(max_length=100, null=True)

    creator = models.ForeignKey('Organiser', null=True, on_delete=models.CASCADE)
    creator_voted = models.BooleanField(default=False)
    created = models.BooleanField(default=False)

    finished = models.BooleanField(default=False)
    skipped = models.BooleanField(default=False)
    notified = models.BooleanField(default=False)
    referee = models.ForeignKey('Referee', on_delete=models.SET_NULL, null=True)
    winner = models.ForeignKey('Participant', related_name='winner_set', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return '%d %s %s' % (self.id, str(self.district), self.time.strftime('%H:%M %d.%m.%y'))

    @property
    def is_full(self):
        return self.participantinevent_set.count() >= self.max_participants

    def format(self, replicas, spec, plain=False, user=None):
        if self.creator:
            return replicas.poster_format % (self.id,
                                             self.time.strftime('%H:%M %d.%m.%y'),
                                             self.town,
                                             self.participantinevent_set.count())
        else:
            if spec == 'referee':
                referee_income = (self.participantinevent_set.count() * self.input_sum) * org_present_referee_income / 100
                if user:
                    status = replicas.choose_type_referee if self.referee == user.referee else replicas.choose_type_registered_referee_awaiting
                    if status == replicas.choose_type_referee:
                        if self.winner:
                            status += ' %s' % replicas.winner_selected
                        elif self.skipped:
                            status += ' %s' % replicas.event_skipped_referee_status_text
                else:
                    status = ''

                participants_text = ''
                participations = self.participantinevent_set.all()
                for participant in participations:
                    participants_text += participant.format(replicas, spec='referee_passed') + '\n'

                if self.winner:
                    winner = self.participantinevent_set.get(participant_id=self.winner_id)
                    winner_text = winner.format(replicas, spec='referee_passed')
                else:
                    winner_text = ''
                return replicas.referee_event_format % (self.id,
                                                        self.time.strftime('%H:%M %d.%m.%y'),
                                                        self.town,
                                                        self._get_text_type(replicas),
                                                        referee_income,
                                                        participants_text,
                                                        winner_text,
                                                        status)

            elif spec == 'find':
                return replicas.find_event_format % (self.id,
                                                     self.time.strftime('%H:%M %d.%m.%y'),
                                                     self.town,
                                                     self.participantinevent_set.count(),
                                                     self.max_participants,
                                                     self._get_text_type(replicas))

            elif spec == 'pre':
                return replicas.pre_event_format % (self.id,
                                                    self.town,
                                                    self.time.strftime('%H:%M %d.%m.%y'))
            elif spec == 'short':
                return replicas.pre_event_format % (self.id,
                                                    self.town,
                                                    self.time.strftime('%H:%M %d.%m.%y'))

    def involved_format(self, user, status, plain=False):
        replicas = user.replicas
        if self.creator:
            return replicas.poster_format % (self.id,
                                             self.time.strftime('%H:%M %d.%m.%y'),
                                             self.town,
                                             self.participantinevent_set.count())
        else:
            return replicas.event_format % (self.id,
                                            self.town,
                                            self.district.name(user),
                                            status,
                                            self.participantinevent_set.count(),
                                            self.max_participants,
                                            self._get_text_type(replicas, plain),
                                            self.time.strftime('%H:%M %d.%m.%y'),
                                            self.get_referee_login(replicas))

    def _get_text_type(self, replicas, plain=False):
        if not plain:
            if self.race_type == 1:
                race_type = replicas.drug_race_text
            elif self.race_type == 2:
                race_type = replicas.street_race_text
            elif self.race_type == 3:
                race_type = replicas.trophy_race_text
            elif self.race_type == 4:
                race_type = replicas.tag_of_war_race_text
            else:
                race_type = replicas.drug_race_text
        else:
            if self.race_type == 1:
                race_type = replicas.plain_drug_race_text
            elif self.race_type == 2:
                race_type = replicas.plain_street_race_text
            elif self.race_type == 3:
                race_type = replicas.plain_trophy_race_text
            elif self.race_type == 4:
                race_type = replicas.plain_tag_of_war_race_text
            else:
                race_type = replicas.plain_drug_race_text

        return race_type

    def get_cars_text(self, replicas):
        participants = self.participantinevent_set.all()
        cars = ''
        for participant in participants:
            cars += participant.car.format(replicas) + '\n'
        return replicas.car_list_format % cars

    def get_role(self, user: User):
        is_organiser = False
        if self.creator:
            is_organiser = user.organiser.id == self.creator.id
        is_referee = False
        is_reg_referee = False
        if self.referee:
            is_referee = user.referee.id == self.referee.id
        else:
            is_reg_referee = user.referee.id in list(
                map(lambda reg_ref: reg_ref.referee.id, self.registeredreferee_set.all()))
        is_participant = user.participant.id in list(map(lambda participation: participation.participant.id,
                                                         self.participantinevent_set.all()))

        if is_participant:
            return user.replicas.choose_type_participant
        elif is_referee:
            return user.replicas.choose_type_referee
        elif is_reg_referee:
            return user.replicas.choose_type_registered_referee
        elif is_organiser:
            return user.replicas.choose_type_org
        else:
            return str(None)

    def set_up_referee(self, referee):
        if not (self.finished or self.referee):
            user = referee.referee.user
            self.referee = referee.referee
            bot.send_message(user.id,
                             user.replicas.referee_notification + self.involved_format(
                                 user,
                                 user.replicas.choose_type_referee))
            bot.send_message(user.id, user.replicas.inform_referee)
            self.save()

            participants = self.participantinevent_set.all()
            phones = ''
            for participant in participants:
                user = participant.participant.user
                bot.send_message(user.id, user.replicas.referee_elected % (self.format(user.replicas, spec='pre'),
                                                                           referee.format(user.replicas,
                                                                                          spec='elected')))
                phones += '%s %s\n' % (user.login, user.phone)

            bot.send_message(referee.referee.user.id, phones)
        else:
            raise ValueError('Wrong data to set_up_referee function, finished event or referee elected')

    def get_referee_login(self, replicas):
        if self.referee:
            return self.referee.user.login
        else:
            return replicas.not_selected

    def can_register_as_referee(self, user: User):
        if user.referee.registeredreferee_set.filter(event=self).first():
            return False
        elif user.participant.participantinevent_set.filter(event=self).first():
            return False
        return True

    def register_as_referee(self, user):
        if user.balance < self.input_sum:
            raise ValueError("Not enough money")
        from bot.utils.to_page_actions import send_referee_voting
        if self.can_register_as_referee(user):

            user.balance -= self.input_sum
            user.save()
            registered_referee = RegisteredReferee(referee=user.referee, event=self)
            registered_referee.save()

            participants = self.participantinevent_set.all()
            for participant in participants:
                user = participant.participant.user
                role = self.get_role(user)
                send_referee_voting(user, registered_referee, role)
            return True
        else:
            return False

    def can_participate(self, car: RaceCar = None, user: User = None):
        if car is None and user is None:
            raise ValueError("One of the user or car have to be specified")
        else:
            if car is not None and user is not None and car not in user.race_cars.all():
                raise ValueError("If specified both user and car then car have to be in propertyof user")
            if car is not None:
                owner = car.owner
            else:
                owner = user
        replicas = owner.replicas
        reasons = []
        status = True

        if self.is_full:
            status = False
            reasons.append(replicas.full_participants_stack_already)
        if owner.balance < self.input_sum:
            status = False
            reasons.append(replicas.not_enough_money)
        if owner.participant.id in list(
                map(lambda part: part.participant.id,
                    self.participantinevent_set.all())) and \
                not owner.referee.id in list(
                    map(lambda reg_ref: reg_ref.referee.id, self.registeredreferee_set.all())):
            status = False
            reasons.append(replicas.participate_already)

        if car:
            if self.participantinevent_set.count() != 0:
                first_car = self.participantinevent_set.order_by('applied_at').first().car
                if self.race_type == 1:
                    volume_max_difference = 0.2
                    power_max_difference = 20

                    if car.engine_volume - first_car.engine_volume > volume_max_difference:
                        status = False
                        reasons.append(
                            replicas.participation_wrong_car_volume % (first_car.engine_volume + volume_max_difference))
                    if car.power - first_car.power > power_max_difference:
                        status = False
                        reasons.append(
                            replicas.participation_wrong_car_power % (first_car.power + power_max_difference))
                    if car.drive_unit != first_car.drive_unit:
                        status = False
                        reasons.append(replicas.participation_wrong_car_drive_unit % car.get_drive_unit_text(replicas))
                    if car.transmission != first_car.transmission:
                        status = False
                        reasons.append(
                            replicas.participation_wrong_car_transmission % car.get_transmission_text(replicas))
                elif self.race_type == 2:
                    power_max_difference = 50

                    if car.power - first_car.power > power_max_difference:
                        status = False
                        reasons.append(
                            replicas.participation_wrong_car_power % (first_car.power + power_max_difference))
                elif self.race_type == 4:
                    volume_max_difference = 0.5

                    if car.engine_volume - first_car.engine_volume > volume_max_difference:
                        status = False
                        reasons.append(
                            replicas.participation_wrong_car_volume % (first_car.engine_volume + volume_max_difference))
                    if car.drive_unit != first_car.drive_unit:
                        status = False
                        reasons.append(replicas.participation_wrong_car_drive_unit % car.get_drive_unit_text(replicas))

        return Availability(status, reasons)

    def participate(self, user, car):
        can_participate = self.can_participate(car)
        if can_participate.status:
            user.balance -= self.input_sum
            participation = ParticipantInEvent(event=self, participant=user.participant, car=car)
            participation.save()
            user.save()
            self.notify_about_new_member()
        if self.is_full:
            self.notify_about_full()
        return can_participate

    def notify_about_full(self):
        participants = self.participantinevent_set.all()
        for p in participants:
            user = p.participant.user
            bot.send_message(user.id,
                             user.replicas.full_participants_notification + '\n\n' + self.format(user.replicas,
                                                                                         spec='short'))
        if self.referee:
            referee = self.referee
            bot.send_message(referee.user.id,
                             referee.user.replicas.full_participants_notification + '\n\n' + self.format(
                                 referee.user.replicas, spec='short'))

    def notify_about_new_member(self):
        participants = self.participantinevent_set.all()
        for p in participants:
            user = p.participant.user
            bot.send_message(user.id,
                             user.replicas.new_participant_notification + '\n\n' + self.format(user.replicas,
                                                                                                 spec='short'))
        if self.referee:
            referee = self.referee
            bot.send_message(referee.user.id,
                             referee.user.replicas.new_participant_notification + '\n\n' + self.format(
                                 referee.user.replicas,
                                 spec='short'))

    def can_leave(self, user):
        status = True
        reasons = []
        if user.referee == self.referee:
            days = 3
            if self.time - timedelta(days=days) < timezone.now():
                status = False
                reasons.append(user.replicas.cant_leave_event % days)
        return Availability(status, reasons)

    def leave(self, user):
        availability = self.can_leave(user)
        if availability.status:
            if self.referee == user.referee:
                self.referee = None
                self.registeredreferee_set.get(referee__user=user).delete()
                self.save()
                participates = self.participantinevent_set.all()
                for participation in participates:
                    participation.voted = False
                    participation.save()
                    user = participation.participant.user
                    markup = types.InlineKeyboardMarkup()
                    markup.add(types.InlineKeyboardButton(user.replicas.elect_referee,
                                                          callback_data='responses.accept_referee.%d' % self.id))
                    bot.send_message(user.id, user.replicas.referee_left_notification % self.format(user.replicas, spec='short'), reply_markup=markup)
                reg_referees = self.registeredreferee_set.all()
                for reg_ref in reg_referees:
                    user = reg_ref.referee.user
                    bot.send_message(user.id, user.replicas.referee_left_notification % self.format(user.replicas, spec='short'))
            else:
                self.registeredreferee_set.get(referee__user=user).delete()
        return availability


class ParticipantInEvent(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    participant = models.ForeignKey('Participant', on_delete=models.CASCADE)
    car = models.ForeignKey('RaceCar', on_delete=models.CASCADE, null=True)
    voted = models.BooleanField(default=False)
    applied_at = models.DateTimeField(auto_now_add=True)

    def format(self, replicas, spec='default'):
        if spec == 'default':
            all_participation = self.participant.participantinevent_set.filter(event__finished=True).count()
            wins = self.participant.participantinevent_set.filter(event__winner=self.participant).count()
            age_gender_text = ''
            if self.participant.user.gender:
                age_gender_text += replicas.gender_format % self.participant.user.gender

            if self.participant.user.age:
                if self.participant.user.gender:
                    age_gender_text += ', '
                age = self.participant.user.age
                if self.participant.user.lang == rus:
                    reminder = age % 10
                    if reminder == 1:
                        year_name = 'Год'
                    elif 2 <= reminder <= 4:
                        year_name = 'Года'
                    else:
                        year_name = 'Лет'
                    age_gender_text += replicas.age_format % (age, year_name)
                else:
                    age_gender_text += replicas.age_format % (age, 'years')
                # age_gender_text += replicas.age_format % self.participant.user.age

            return replicas.participant_format % (self.participant.user.login,
                                                  age_gender_text,
                                                  self.car.format(replicas),
                                                  all_participation,
                                                  wins,
                                                  all_participation - wins,
                                                  self.participant.rating,
                                                  )
        elif spec == 'referee_passed':
            return replicas.referee_participant_format % (self.participant.user.login,
                                                          self.car.brand,
                                                          self.car.color,
                                                          self.participant.user.gender,
                                                          self.participant.user.age)


class RegisteredReferee(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    referee = models.ForeignKey('Referee', on_delete=models.CASCADE)
    votes = models.IntegerField(default=0)

    def format(self, replicas, spec='org'):
        if spec == 'org' or spec == 'participant':
            return replicas.registered_referee_format % (self.referee.user.login,
                                                         self.referee.rating,
                                                         self.referee.user.gender,
                                                         self.referee.user.age,
                                                         self.referee.event_set.count())

        if spec == 'elected':
            return replicas.elected_referee_format % (self.referee.user.login,
                                                      self.referee.user.phone)


class Participant(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rating = models.FloatField(default=0)
    voted_times = models.IntegerField(default=0)


class Referee(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rating = models.FloatField(default=0)
    voted_times = models.IntegerField(default=0)


class Organiser(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rating = models.FloatField(default=0)
    voted_times = models.IntegerField(default=0)


class Time(BaseModel):
    """
    This is singleton model, for remembering, when last payeer check was performed
    """
    time = models.DateTimeField()


class TaxiDriver(BaseModel):
    rating = models.FloatField(default=0)
    voted_times = models.IntegerField(default=0)
    user = models.OneToOneField('User', on_delete=models.CASCADE)
    price = models.FloatField(null=True)


class TaxiOrder(BaseModel):
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='client_for_orders')
    car = models.ForeignKey('TaxiCar', on_delete=models.SET_NULL, null=True)

    hours = models.IntegerField(null=True)
    total_price = models.FloatField(null=True)

    address = models.TextField(null=True)
    time = models.DateTimeField(default=datetime.now)
    created = models.DateTimeField(null=True)
    min_price = models.FloatField(null=True)
    max_price = models.FloatField(null=True)
    town = models.CharField(max_length=50)
    district = models.ForeignKey('Region', null=True, on_delete=models.SET_NULL, related_name='orders_set')

    voted = models.BooleanField(default=False)
    accepted = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    payed_drivers = models.ManyToManyField('User', related_name='pay_for_orders')
    last_notification = models.DateTimeField(null=True)

    def get_price(self, car=None):
        if car:
            return car.get_price(self.hours)
        return self.car.get_price(self.hours)

    def format(self, user, spec='short', message=None):
        replicas = user.replicas
        if spec == 'short':
            return replicas.order_format % (self.id,
                                            self.time.strftime('%H:%M %d.%m.%y'),
                                            self.town)
        elif spec == 'full':
            return replicas.full_order_format % (self.id,
                                                 self.time.strftime('%H:%M %d.%m.%y'),
                                                 self.town,
                                                 self.address,
                                                 self.hours,
                                                 self.user.phone)
        elif spec == 'client':
            if not message:
                message = user.replicas.new_order_response
            if self.car:
                return replicas.client_order_format % (message,
                                                       self.id,
                                                       self.car.brand,
                                                       self.car.price,
                                                       self.car.driver.taxidriver.rating,
                                                       self.car.driver.phone)
            else:
                bot.send_message(121193878, 'No car at order')
                bot.send_message(121193878, str(self.__dict__))
                bot.send_message(121193878, str(self.user.__dict__))
        elif spec == 'driver_notification_short':
            return replicas.new_order_format % (self.district.name(user),
                                                self.town,
                                                self.time.strftime('%H:%M %d.%m.%y'))
        elif spec == 'driver_notification_full':
            return replicas.new_order_full_format % (self.district.name(user),
                                                     self.town,
                                                     self.time.strftime('%H:%M %d.%m.%y'),
                                                     self.address,
                                                     timezone.localtime(self.created).strftime('%H:%M %d.%m.%y'),
                                                     self.hours,
                                                     self.user.age,
                                                     self.user.gender)

    def __str__(self):
        return ' %d' % self.id


class Translatable(BaseModel):
    russian_name = models.CharField(max_length=100)
    english_name = models.CharField(max_length=100)

    def name(self, user: User):
        if user.lang == rus:
            return self.russian_name
        elif user.lang == eng:
            return self.english_name

    @classmethod
    def get_by_name(cls, name):
        return cls.objects.get(Q(russian_name=name) | Q(english_name=name))

    def __repr__(self):
        return '%s %s' % (self.russian_name, self.__class__.__name__)

    __str__ = __repr__


class Country(Translatable):
    continent = models.IntegerField(default=1)
    pass


class Region(Translatable):
    country_object = models.ForeignKey('Country', on_delete=models.CASCADE)
