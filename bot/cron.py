from bot.controllers.event import elect_referees, skip_selecting_winner, finish_skipped_events, notify_referees
from bot.controllers.taxi import return_charges
from bot.controllers.payeer import run as check_payeer


def minute_script():
    try:
        elect_referees()
    except Exception as e:
        print(e)
    try:
        return_charges()
    except Exception as e:
        print(e)
    try:
        skip_selecting_winner()
    except Exception as e:
        print(e)
    try:
        finish_skipped_events()
    except Exception as e:
        print(e)
    try:
        check_payeer()
    except Exception as e:
        print(e)


def daily_script():
    notify_referees()

