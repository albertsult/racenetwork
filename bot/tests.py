import random
import string

from django.test import TestCase

from bot.models import User, Event, RaceCar, ParticipantInEvent, Participant, Referee


def id_generator(size=6, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class TestTest(TestCase):
    def test_addition(self):
        self.assertEqual(2 + 2, 4)


class CanParticipateTests(TestCase):
    def setUp(self):
        User.objects.create(id=1234567, state=0, lang='Русский 🇷🇺', balance=1000)
        Participant.objects.create(user_id=1234567)
        Referee.objects.create(user_id=1234567)

        User.objects.create(id=7654321, state=0, lang='Русский 🇷🇺', balance=1000)
        Participant.objects.create(user_id=7654321)
        Referee.objects.create(user_id=7654321)

    def test_first_car(self):
        event = Event(race_type=1, input_sum=200, max_participants=10)
        car = RaceCar(engine_volume=50, owner_id=1234567)
        result = event.can_participate(car)
        self.assertTrue(result.status)

    def test_not_enough_money(self):
        event = Event(race_type=1, input_sum=2000000)
        car = RaceCar(engine_volume=50, owner_id=1234567)
        result = event.can_participate(car)
        self.assertFalse(result.status)

    def test_wrong_car_drug_type_race(self):
        event = Event(race_type=1, input_sum=200, max_participants=10)
        event.save()
        origin_car = RaceCar(engine_volume=50, owner_id=1234567)
        origin_car.save()
        test_car = RaceCar(engine_volume=60, owner_id=1234567)
        participant = ParticipantInEvent(event=event, car=origin_car, participant_id=1)
        participant.save()
        result = event.can_participate(test_car)
        self.assertFalse(result.status)

    def test_right_car_drug_type_race(self):
        event = Event(race_type=1, input_sum=200, max_participants=10)
        event.save()
        origin_car = RaceCar(engine_volume=50, owner_id=1234567)
        origin_car.save()
        test_car = RaceCar(engine_volume=49, owner_id=7654321)
        participant = ParticipantInEvent(event=event, car=origin_car, participant_id=1)
        participant.save()
        result = event.can_participate(test_car)
        self.assertTrue(result.status)
