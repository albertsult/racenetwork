import io
from itertools import chain

import requests
from PIL import Image
from django.utils.timezone import now
from telebot import types

from bot import bot
from bot.models import User, RaceCar, TaxiCar, ParticipantInEvent, Country, Event, RegisteredReferee
from bot.services import get_responses_list
from bot.utils.utils import create_photo_with_description
from config import telegram_file_link, token
from messages import messages


def ask_for_language(user, first=False):
    if first:
        message = 'Choose your language'
    else:
        message = user.replicas.choose_language
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    for lang_code, lang_obj in messages.items():
        markup.add(types.KeyboardButton(lang_code))
    bot.send_message(user.id, message, reply_markup=markup)


def to_agreement_page(user):
    user.state = 4
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.agreed))
    bot.send_message(user.id, user.replicas.agreement_conditions, reply_markup=markup)
    user.save()


def to_main_page(user: User, message=None):
    if message is None:
        message = user.replicas.start_message

    user.state = 1
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    profile = types.KeyboardButton(user.replicas.main_menu_profile)
    events = types.KeyboardButton(user.replicas.main_menu_events)
    taxi = types.KeyboardButton(user.replicas.taxi_menu_order_taxi)
    invite = types.KeyboardButton(user.replicas.main_menu_invite_friend)

    markup.add(profile, events, taxi, invite)
    bot.send_message(user.id, message, reply_markup=markup)
    user.save()


def to_events_page(user: User, message=None, pagination_buttons=False):
    if message is None:
        message = user.replicas.events_page_description

    user.state = 2
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)

    responses = types.KeyboardButton(messages[user.lang].events_menu_responses)
    event_search = types.KeyboardButton(messages[user.lang].events_menu_event_search)
    event_create = types.KeyboardButton(messages[user.lang].events_menu_event_create)
    top_winners = types.KeyboardButton(user.replicas.best_winners)

    back = types.KeyboardButton(user.replicas.back_text)
    if pagination_buttons:
        markup.add(types.KeyboardButton('<<'),
                   types.KeyboardButton('>>'))
    markup.add(event_create, event_search, responses, top_winners, back)
    bot.send_message(user.id, message, reply_markup=markup)
    user.save()


def to_profile_page(user, message=None):
    user.state = 10

    if message is None:
        message = '✔️'

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    change_language = types.KeyboardButton(messages[user.lang].change_language_button)
    taxi_driver = types.KeyboardButton(user.replicas.choose_type_taxi_driver)
    my_town = types.KeyboardButton(messages[user.lang].my_town_button)
    participant = types.KeyboardButton(messages[user.lang].choose_type_participant)
    referee = types.KeyboardButton(messages[user.lang].choose_type_referee)
    contact = types.KeyboardButton(user.replicas.send_contact_button, request_contact=True)
    my_login = types.KeyboardButton(user.replicas.my_login_button)
    gender = types.KeyboardButton(user.replicas.gender_button_text)
    age = types.KeyboardButton(user.replicas.age_button_text)

    # profile_type = types.KeyboardButton(messages[user.lang].profile_type_button)
    my_balance = types.KeyboardButton(messages[user.lang].my_balance_button)
    # about = types.KeyboardButton(user.replicas.about_button_text)
    back = types.KeyboardButton(user.replicas.back_text)
    markup.add(my_balance, referee, participant, my_town, age, change_language, contact, my_login, gender, taxi_driver,
               back)
    bot.send_message(user.id, message, reply_markup=markup)
    user.save()


def to_login_page(user, message=None):
    """Since this function is used for in bot
     context. It is not need save in the end"""

    if not message:
        message = messages[user.lang].current_login % user.login
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if not user.login:
        user.state = 14
        markup.add(types.KeyboardButton(messages[user.lang].back_text))
        bot.send_message(user.id, messages[user.lang].ask_for_login, reply_markup=markup)
    else:
        user.state = 13
        markup.add(types.KeyboardButton(messages[user.lang].change_login_button),
                   types.KeyboardButton(messages[user.lang].back_text),
                   types.KeyboardButton(user.replicas.to_main_menu_button_text))
        bot.send_message(user.id, message, reply_markup=markup, parse_mode='Markdown')


# def to_choose_profile_type(user, message=None):
#     if not message:
#         message = user.replicas.choose_profile_type_description
#     user.state = 12
#     markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
#     participant = types.KeyboardButton(messages[user.lang].choose_type_participant)
#     referee = types.KeyboardButton(messages[user.lang].choose_type_referee)
#     # organiser = types.KeyboardButton(messages[user.lang].choose_type_org)
#     back = types.KeyboardButton(messages[user.lang].back_text)
#     main_page = types.KeyboardButton(messages[user.lang].to_main_menu_button_text)
#
#     # profile_type = types.KeyboardButton(messages[user.lang].profile_type_button)
#     # my_balance = types.KeyboardButton(messages[user.lang].my_balance_button)
#     markup.add(participant, referee, back, main_page)
#     bot.send_message(user.id, message, reply_markup=markup)
#     user.save()


def to_participant_page(user, message=None):
    user.state = 80
    if message is None:
        message = user.replicas.participant_page_description

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    stat = types.KeyboardButton(messages[user.lang].stat_button_text)
    add_cars = types.KeyboardButton(user.replicas.add_car_button_text)

    # taxi_driver = types.KeyboardButton(user.replicas.choose_type_taxi_driver)

    back = types.KeyboardButton(messages[user.lang].back_text)
    main_menu = types.KeyboardButton(messages[user.lang].to_main_menu_button_text)

    markup.add(stat, add_cars, back, main_menu)
    bot.send_message(user.id, message, reply_markup=markup)

    cars = user.race_cars.filter(created=True).all()
    for car in cars:
        if car.photo_id:
            try:
                url = telegram_file_link % (token, bot.get_file(car.photo_id).file_path)
                r = requests.get(url)
                bot.send_photo(user.id, photo=r.content)
            except Exception as e:
                print(e)

        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(user.replicas.delete, callback_data='race_car.delete.%d' % car.id))
        bot.send_message(user.id, car.format(user.replicas), reply_markup=markup)

    user.save()


def to_referee_page(user, message):
    user.state = 60
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    stat = types.KeyboardButton(messages[user.lang].stat_button_text)
    passed_events = types.KeyboardButton(messages[user.lang].passed_events_button_text)
    back = types.KeyboardButton(messages[user.lang].back_text)
    main_menu = types.KeyboardButton(messages[user.lang].to_main_menu_button_text)

    markup.add(stat, passed_events, back, main_menu)
    bot.send_message(user.id, message, reply_markup=markup)
    user.save()


# def to_about_page(user: User):
#     user.state = 19
#     markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
#     contact = types.KeyboardButton(user.replicas.send_contact_button, request_contact=True)
#     my_login = types.KeyboardButton(user.replicas.my_login_button)
#     gender = types.KeyboardButton(user.replicas.gender_button_text)
#     age = types.KeyboardButton(user.replicas.age_button_text)
#     back = types.KeyboardButton(messages[user.lang].back_text)
#     main_menu = types.KeyboardButton(messages[user.lang].to_main_menu_button_text)
#
#     markup.add(contact, my_login, gender, age, back, main_menu)
#     bot.send_message(user.id, user.replicas.about_page_description, reply_markup=markup)
#     user.save()


def to_org_page(user, message):
    user.state = 70
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    stat = types.KeyboardButton(messages[user.lang].stat_button_text)
    back = types.KeyboardButton(messages[user.lang].back_text)
    main_menu = types.KeyboardButton(messages[user.lang].to_main_menu_button_text)

    markup.add(stat, back, main_menu)
    bot.send_message(user.id, message, reply_markup=markup)
    user.save()


def to_taxi_driver_page(user, messages_list=None, offer_car=True, add_car=True, select_button=True, delete_button=True):
    user.state = 120
    cars = user.taxi_cars.filter(created=True).all()

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if offer_car:
        markup.add(types.KeyboardButton(user.replicas.taxi_menu_find_orders))
    if add_car:

        markup.add(types.KeyboardButton(user.replicas.add_car_button_text))
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))

    if messages_list:
        for message in messages_list:
            bot.send_message(user.id, message, reply_markup=markup)
    else:
        bot.send_message(user.id, '✔', reply_markup=markup)
    if select_button:
        # bot.send_message(user.id, user.replicas.order_price_notification, reply_markup=markup)
        if cars.count() > 0:
            for car in cars:
                buttons = []
                if delete_button:
                    buttons.append(
                        types.InlineKeyboardButton(user.replicas.delete, callback_data='taxi.car.delete.%d' % car.id))
                car.send_to_driver(user, buttons)
        else:
            bot.send_message(user.id, user.replicas.no_car_found)
    else:
        for car in cars:
            car.send_to_driver(user, [])


    # bot.send_message(user.id, '✔️', reply_markup=markup)


#
# def to_event_page(user, message='✔️'):
#     user.state = 30
#     markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
#     create_event = types.KeyboardButton(messages[user.lang].create_event_text)
#     event_list = types.KeyboardButton(messages[user.lang].event_list_button_text)
#     top_winners = types.KeyboardButton(user.replicas.best_winners)
#     back_button = types.KeyboardButton(messages[user.lang].back_text)
#     markup.add(create_event, event_list, top_winners, back_button)
#     bot.send_message(user.id, message, reply_markup=markup)
#     user.save()

def to_choose_continent(user, state, message=None):
    if not message:
        message = '✔️'

    user.state = state
    user.save()

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.europe_button),
               types.KeyboardButton(user.replicas.asia_button),
               types.KeyboardButton(user.replicas.north_america_button),
               types.KeyboardButton(user.replicas.south_america_button),
               types.KeyboardButton(user.replicas.africa_button),
               )

    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, message, reply_markup=markup)


def to_choose_country(user, continent, state, message=None):
    if not message:
        message = '✔️'

    user.state = state
    # user.state_after_location = state
    user.save()

    countries = Country.objects.filter(continent=continent)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    buttons = []
    for country in countries:
        buttons.append(types.KeyboardButton(country.name(user)))
    markup.add(*buttons)
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, message, reply_markup=markup)


def to_choose_region(user: User, state, country=None, message=None, choose_continent_state=16, need_to_main_menu=True):
    if not country:
        district = user.district
        if district:
            country = district.country_object
        else:
            to_choose_continent(user, state=choose_continent_state, message=user.replicas.country_not_selected)
            return

    if not message:
        message = user.replicas.ask_for_location_district

    user.state = state
    user.save()

    regions = country.region_set.all()
    if user.district:
        if country.id == user.district.country_object.id:
            regions = regions.exclude(id=user.district.id)
            regions = list(chain([user.district], regions))

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    buttons = []
    for region in regions:
        buttons.append(types.KeyboardButton(region.name(user)))
    markup.add(*buttons)
    markup.add(types.KeyboardButton(user.replicas.back_text))
    if need_to_main_menu:
        markup.add(types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, message, reply_markup=markup)


def to_town_page(user, message=None):
    if not message:
        message = user.replicas.location_format % (user.district.name(user), user.town)
    user.state = 15
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(messages[user.lang].change_town_button),
               types.KeyboardButton(messages[user.lang].back_text),
               types.KeyboardButton(messages[user.lang].to_main_menu_button_text))
    bot.send_message(user.id, message, reply_markup=markup)


def to_register_in_event(user, event):
    user.state = 50
    event_desc = messages[user.lang].event_format % (event.time.strftime('%H:%M %d.%m.%y'),
                                                     event.place,
                                                     event.input_sum,
                                                     event.max_participants,
                                                     messages[user.lang].round_race_text if event.race_type == 0 else
                                                     messages[user.lang].drug_race_text,
                                                     )
    bot.send_message(user.id, event_desc)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    ok = types.KeyboardButton(messages[user.lang].ok_text)
    back = types.KeyboardButton(messages[user.lang].back_text)
    markup.add(ok, back)
    bot.send_message(user.id, messages[user.lang].ask_to_confirm_participate_in_event, reply_markup=markup)
    user.save()


def to_create_race_car_page(user):
    user.state = 82
    new_car = RaceCar(owner=user)
    new_car.save()
    user.creating_race_car = new_car
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, user.replicas.ask_for_model_and_brand, reply_markup=markup)
    user.save()


def to_create_taxi_car_page(user):
    user.state = 121
    new_taxi = TaxiCar(driver=user)
    new_taxi.save()
    user.creating_taxi_car = new_taxi
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, user.replicas.ask_for_model_and_brand, reply_markup=markup)
    user.save()


def to_balance_page(user, message=None):
    user.state = 90
    if message:
        bot.send_message(user.id, message)
    answer = user.replicas.balance_page_description % user.balance
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.add_money_button_text),
               types.KeyboardButton(user.replicas.get_money_button_text),
               types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, answer, reply_markup=markup)
    user.save()


def to_responses_page(user):
    bot.send_message(user.id, user.replicas.responses_page_description)
    responses = get_responses_list(user)
    response = responses.last()
    if response:
        send_response_with_pagination(user, response)
    else:
        bot.send_message(user.id, user.replicas.no_events_found)


def send_response_with_pagination(user: User, event: Event):
    user.state = 26
    if event.photo_id:
        try:
            status = event.get_role(user)
            markup = types.InlineKeyboardMarkup()

            if not event.creator:
                if status != user.replicas.choose_type_referee and status != user.replicas.choose_type_registered_referee:
                    if not event.referee:
                        markup.add(types.InlineKeyboardButton(user.replicas.elect_referee,
                                                              callback_data='responses.accept_referee.%d' % event.id))
                else:
                    markup.add(
                        types.InlineKeyboardButton(user.replicas.leave, callback_data='responses.leave.%d' % event.id))
                if status == user.replicas.choose_type_participant and event.referee and now().date() == event.time.date():
                    markup.add(
                        types.InlineKeyboardButton(user.replicas.arrived,
                                                   callback_data='responses.arrived.%d' % event.id))

            answer = event.involved_format(user, event.get_role(user), plain=True)
            url = telegram_file_link % (token, bot.get_file(event.photo_id).file_path)
            r = requests.get(url)
            image = Image.open(io.BytesIO(r.content))
            res_image = create_photo_with_description(answer, image, event.creator)
            img_byte_arr = io.BytesIO()
            res_image.save(img_byte_arr, format='PNG')
            bot.send_photo(user.id, photo=img_byte_arr.getvalue(), reply_markup=markup)
        except ValueError as e:
            print(e)
    # else:
    #     bot.send_message(user.id, '✔', reply_markup=markup)
    user.current_response_in_list = event
        # to_details_response_view(user, event)

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.prev_response),
               types.KeyboardButton(user.replicas.next_response))
    # markup.add(types.KeyboardButton(user.replicas.pilots_button))
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, '✔', reply_markup=markup)
    user.save()

def to_choose_date(user, state=None):
    if state:
        user.state = state
    markup = types.InlineKeyboardMarkup(row_width=5)
    markup.add(types.InlineKeyboardButton('2018', callback_data='choose_date.year.%d' % 2018),
               types.InlineKeyboardButton('2019', callback_data='choose_date.year.%d' % 2019),
               types.InlineKeyboardButton('2020', callback_data='choose_date.year.%d' % 2020),
               types.InlineKeyboardButton('2021', callback_data='choose_date.year.%d' % 2021),
               types.InlineKeyboardButton('2022', callback_data='choose_date.year.%d' % 2022), )
    bot.send_message(user.id, user.replicas.choose_date, reply_markup=markup)


def to_select_winner_message(user, message, event):
    participants = event.participantinevent_set.all()
    if len(participants) == 0:
        bot.edit_message_text(messages[user.lang].no_users_found, user.id, message.message_id)
    else:
        markup = types.InlineKeyboardMarkup(row_width=1)
        for participant in participants:
            markup.add(types.InlineKeyboardButton(str(participant.participant.user.login),
                                                  callback_data='referee.select_winner.%d.%d' % (
                                                      event.id, participant.participant.id)))
        bot.edit_message_text(messages[user.lang].select_winner, user.id, message.message_id, reply_markup=markup)


def to_create_event_page(user: User, message=None):
    if not message:
        message = '✔️'
    user.state = 30
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    markup.add(types.KeyboardButton(user.replicas.set_event))
    markup.add(types.KeyboardButton(user.replicas.create_event_as_participant_text))
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, message, reply_markup=markup)


def to_best_winners_page(user, message=None):
    if not message:
        message = '🔝'
    user.state = 22
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    markup.add(types.KeyboardButton(user.replicas.drug_race_text),
               types.KeyboardButton(user.replicas.street_race_text),
               types.KeyboardButton(user.replicas.trophy_race_text),
               types.KeyboardButton(user.replicas.tag_of_war_race_text),
               types.KeyboardButton(user.replicas.back_text))
    bot.send_message(user.id, message, reply_markup=markup)


def to_town_input_page(user, need_to_main_menu, next_state=None, message=None):
    if next_state:
        user.state = next_state

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.back_text))
    if need_to_main_menu:
        markup.add(types.KeyboardButton(user.replicas.to_main_menu_button_text))
    if not message:
        message = user.replicas.ask_for_town
    bot.send_message(user.id, message, reply_markup=markup)


def to_find_event_list(user, event):
    user.state = 23

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.prev_event),
               types.KeyboardButton(user.replicas.next_event))
    markup.add(types.KeyboardButton(user.replicas.participate),
               types.KeyboardButton(user.replicas.pilots_button))
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))

    if event.photo_id:
        # if user.prev_participant_photo_message_id:
        #     bot.delete_message(user.id, user.prev_participant_photo_message_id, corrupt_exceptions=True)
        try:
            url = telegram_file_link % (token, bot.get_file(event.photo_id).file_path)
            r = requests.get(url)
            image = Image.open(io.BytesIO(r.content))
            answer = event.format(user.replicas, spec='find', plain=True)
            res_image = create_photo_with_description(answer, image, event.creator)
            img_byte_arr = io.BytesIO()
            res_image.save(img_byte_arr, format='PNG')
            bot.send_photo(user.id, photo=img_byte_arr.getvalue(), reply_markup=markup)
        except Exception as e:
            print(e)

    user.save()


def ask_for_phone(user):
    user.state = 1
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.send_contact_button, request_contact=True),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, user.replicas.contact_required, reply_markup=markup)


def to_participant_list_page(user, event, participant: ParticipantInEvent, edit_event=False, state=24, participate=True):
    user.state = state

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if participant:
        markup.add(types.KeyboardButton(user.replicas.prev_pilot),
                   types.KeyboardButton(user.replicas.next_pilot))
    if participate:
        markup.add(types.KeyboardButton(user.replicas.participate))
    markup.add(types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    if participant:
        user.find_event_current_participant = participant

        if participant.car.photo_id:


            url = telegram_file_link % (token, bot.get_file(participant.car.photo_id).file_path)
            r = requests.get(url)
            image = Image.open(io.BytesIO(r.content))
            answer = participant.format(user.replicas)
            res_image = create_photo_with_description(answer, image)
            img_byte_arr = io.BytesIO()
            res_image.save(img_byte_arr, format='PNG')
            bot.send_photo(user.id, photo=img_byte_arr.getvalue())

        bot.send_message(user.id, '✔', reply_markup=markup)


    else:
        answer = user.replicas.no_participants

        bot.send_message(user.id, answer, reply_markup=markup, parse_mode='Markdown')

    user.save()


def to_participate_page(user: User):
    user.state = 25
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton(user.replicas.choose_type_participant),
               types.KeyboardButton(user.replicas.choose_type_referee),
               types.KeyboardButton(user.replicas.back_text),
               types.KeyboardButton(user.replicas.to_main_menu_button_text))
    bot.send_message(user.id, '✔', reply_markup=markup)


def to_details_response_view(user, event):
    markup = types.InlineKeyboardMarkup()
    status = event.get_role(user)
    if not event.creator:
        bot.send_message(user.id, event.get_cars_text(user.replicas))

        if status != user.replicas.choose_type_referee and status != user.replicas.choose_type_registered_referee:
            if not event.referee:
                markup.add(types.InlineKeyboardButton(user.replicas.elect_referee,
                                                      callback_data='responses.accept_referee.%d' % event.id))
        else:
            markup.add(types.InlineKeyboardButton(user.replicas.leave, callback_data='responses.leave.%d' % event.id))
        if status == user.replicas.choose_type_participant and event.referee:
            markup.add(
                types.InlineKeyboardButton(user.replicas.arrived, callback_data='responses.arrived.%d' % event.id))

    answer = event.involved_format(user, status)

    bot.send_message(user.id, answer, reply_markup=markup)


def send_referee_voting(user, referee: RegisteredReferee, role):
    event = referee.event
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton(user.replicas.select,
                                       callback_data='responses.elect_referee.%d.%d' % (event.id,
                                                                                        referee.id)))
    markup.add(types.InlineKeyboardButton(user.replicas.decline,
                                          callback_data='responses.discard_referee.%d.%d' % (event.id,
                                                                                           referee.id)))
    answer = user.replicas.new_referee_notification % (referee.event.id, referee.format(user.replicas, spec='participant'))
    bot.send_message(user.id, answer, reply_markup=markup)


def to_select_gender(user, message=None):
    if not message:
        message = user.replicas.ask_for_gender % user.gender
    user.state = 130
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    male = types.KeyboardButton(user.replicas.male_option_text)
    female = types.KeyboardButton(user.replicas.female_option_text)
    back = types.KeyboardButton(user.replicas.back_text)

    main_menu = types.KeyboardButton(user.replicas.to_main_menu_button_text)

    markup.add(male, female, back, main_menu)
    bot.send_message(user.id, message, reply_markup=markup)


def to_select_age(user, message=None):
    if user.age:
        str_age = str(user.age)
    else:
        str_age = user.replicas.not_specified

    if not message:
        message = user.replicas.ask_for_age % str_age
    user.state = 131
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    back = types.KeyboardButton(user.replicas.back_text)
    main_menu = types.KeyboardButton(user.replicas.to_main_menu_button_text)

    markup.add(back, main_menu)

    bot.send_message(user.id, message, reply_markup=markup)
