import datetime
import logging

from PIL import Image, ImageFont, ImageDraw
from django.utils import timezone

logger = logging.getLogger("utils")


# def get_district_type(district):
#     for place_type, regions in places.items():
#         if district in regions:
#             return place_type
#     logger.warning('Can\'t find district in list| %s' % district)
#     return 'Субъект РФ'


def local_now():
    return timezone.localtime(timezone.now())


def month_from_now():
    return timezone.localtime(timezone.now()) - datetime.timedelta(days=30)


def phone_format(phone):
    return '%s-%s-%s-%s-%s' % (phone[:-10], phone[-10:-7], phone[-7:-4], phone[-4:-2], phone[-2:])


def create_photo_with_description(description, photo, has_creator=False):
    font = ImageFont.truetype('static/emoji.ttf', 18)
    lines = description.split('\n')
    text_widths = []
    text_heights = []
    for line in lines:
        text_width, text_height = font.getsize(line)
        text_widths.append(text_width)
        text_heights.append(text_height)
    if has_creator:
        background_color = (255, 0, 0)
    else:
        background_color = (96, 128, 158)
    text_img = Image.new('RGB', (max(text_widths)+5, sum(text_heights)+20), color=background_color)
    draw = ImageDraw.Draw(text_img)
    current_height = 10
    for line, height in zip(lines, text_heights):
        draw.text((5, current_height), line, font=font, fill=(255, 255, 230))
        current_height += height

    new_im = Image.new('RGB', (max(photo.width, text_img.width), photo.height+text_img.height), color=background_color)
    new_im.paste(photo)
    new_im.paste(text_img, (0, photo.height))
    return new_im


def choose_elem(action, current_id, elements):
    if action == 'next':
        order = elements.filter(id__gt=current_id).first()
        if not order:
            order = elements.first()
    else:
        order = elements.filter(id__lt=current_id).last()
        if not order:
            order = elements.last()
    return order

