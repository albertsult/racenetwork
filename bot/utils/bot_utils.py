from django.core.exceptions import ObjectDoesNotExist
from telebot import types

from bot import bot
from bot.models import Event, User, Country, Region
from bot.services import get_list_of_available_events
from bot.utils.to_page_actions import to_main_page, to_find_event_list, to_town_input_page, to_choose_country
from bot.utils.utils import local_now


def ask_to_rate_involved(event: Event):
    participants = event.participantinevent_set.all()
    referee = event.referee
    organiser = event.creator

    for participant in participants:
        user = participant.participant.user
        bot.send_message(user.id, user.replicas.ask_to_rate % (event.id, event.town))
        for part_to_rate in participants.exclude(id=participant.id):
            ask_to_rate(user, part_to_rate, event, None, is_participant=True)

        ask_to_rate(user, referee, event, 'referee')
        if organiser:
            ask_to_rate(user, organiser, event, 'org')

    user = referee.user
    bot.send_message(user.id, user.replicas.ask_to_rate % (event.id, event.town))
    for part_to_rate in participants:
        ask_to_rate(user, part_to_rate, event, None, is_participant=True)

    if organiser:
        ask_to_rate(user, organiser, event, 'org')

    if organiser:
        user = organiser.user
        bot.send_message(user.id, user.replicas.ask_to_rate % (event.id, event.town))
        for part_to_rate in participants:
            ask_to_rate(user, part_to_rate, event, None, is_participant=True)
        ask_to_rate(user, referee, event, 'referee')


def ask_to_rate(user, involved, event, role, is_participant=False):
    user_to_rate = involved.user if not is_participant else involved.participant.user
    if user_to_rate.id != user.id:
        markup = types.InlineKeyboardMarkup(row_width=1)
        if is_participant:
            markup.add(types.InlineKeyboardButton('⭐️⭐⭐️⭐️⭐️️', callback_data='rate.participant.5.%d.%d' % (
                event.id, involved.id)),
                       types.InlineKeyboardButton('⭐️⭐⭐️⭐️', callback_data='rate.participant.4.%d.%d' % (
                           event.id, involved.id)),
                       types.InlineKeyboardButton('⭐️⭐⭐️', callback_data='rate.participant.3.%d.%d' % (
                           event.id, involved.id)),
                       types.InlineKeyboardButton('⭐️⭐️', callback_data='rate.participant.2.%d.%d' % (
                           event.id, involved.id)),
                       types.InlineKeyboardButton('⭐️', callback_data='rate.participant.1.%d.%d' % (
                           event.id, involved.id)), )
            bot.send_message(user.id,
                             user.replicas.ask_to_rate_participant % (
                                 involved.participant.user.login, involved.car.brand),
                             corrupt_exceptions=True,
                             reply_markup=markup)
        else:
            markup.add(types.InlineKeyboardButton('⭐️⭐⭐️⭐️⭐️️', callback_data='rate.%s.5.%d' % (role, event.id)),
                       types.InlineKeyboardButton('⭐️⭐⭐️⭐️', callback_data='rate.%s.4.%d' % (role, event.id)),
                       types.InlineKeyboardButton('⭐️⭐⭐️', callback_data='rate.%s.3.%d' % (role, event.id)),
                       types.InlineKeyboardButton('⭐️⭐️', callback_data='rate.%s.2.%d' % (role, event.id)),
                       types.InlineKeyboardButton('⭐️', callback_data='rate.%s.1.%d' % (role, event.id)), )
            bot.send_message(user.id,
                             user.replicas.ask_to_rate_non_participant % (event.get_role(involved.user), involved.user.login),
                             corrupt_exceptions=True,
                             reply_markup=markup)


def process_country_input(user, message, back_redirect, next_state, need_to_main_menu=False):
    if message.text == user.replicas.back_text:
        back_redirect(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
    else:
        try:
            user.state = next_state
            country = Country.get_by_name(message.text)
            regions = country.region_set.all()
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            buttons = []
            for region in regions:
                buttons.append(types.KeyboardButton(region.name(user)))
            markup.add(*buttons)
            markup.add(types.KeyboardButton(user.replicas.back_text))
            if need_to_main_menu:
                markup.add(types.KeyboardButton(user.replicas.to_main_menu_button_text))
            bot.send_message(user.id, user.replicas.ask_for_location_district, reply_markup=markup)
        except ObjectDoesNotExist:
            unknown_action(user)


def process_continent_input(user: User, message, back_redirect, next_state, need_to_main_menu=False):
    if message.text == user.replicas.back_text:
        back_redirect(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
    else:
        if message.text == user.replicas.europe_button:
            continent = 1
        elif message.text == user.replicas.asia_button:
            continent = 2
        elif message.text == user.replicas.north_america_button:
            continent = 3
        elif message.text == user.replicas.south_america_button:
            continent = 4
        elif message.text == user.replicas.africa_button:
            continent = 5
        else:
            continent = None
        if continent:
            to_choose_country(user, continent, state=next_state)
        else:
            unknown_action(user)


def process_district_input(user, message, back_action, district_action=None, is_last=False, need_to_main_menu=False,
                           next_state=None, message_to_send=None):
    """
        This method increments user state by one, you can explicitly set it after this func call
    """
    if message.text == user.replicas.back_text:
        back_action(user)
    elif message.text == user.replicas.to_main_menu_button_text:
        to_main_page(user)
    else:
        try:
            region = Region.get_by_name(message.text)
            if district_action:
                district_action(user, region)
            if not is_last:
                to_town_input_page(user, need_to_main_menu, next_state=next_state, message=message_to_send)
        except ObjectDoesNotExist:
            unknown_action(user)


def list_of_available_events(user: User, district):
    event = get_list_of_available_events(user, district).first()

    if not event:
        bot.send_message(user.id, user.replicas.no_district_events_found)
    else:
        user.find_event_district = district
        user.find_current_event = event

        to_find_event_list(user, event)


def unknown_action(user, message=None):
    if not message:
        message = user.replicas.unknown_text
    bot.send_message(user.id, message)


def gen_referee_stat(user):
    referee = user.referee
    result = user.replicas.referee_stat_format % (referee.event_set.count(), referee.rating)
    return result


def send_passed_event_action(user, events):
    if len(events) == 0:
        bot.send_message(user.id, user.replicas.no_events_found)
    else:
        for event in events:
            markup = types.InlineKeyboardMarkup()
            if event.referee == user.referee and event.time < local_now() and not event.winner and not event.skipped:
                winner = types.InlineKeyboardButton(user.replicas.select_winner,
                                                    callback_data='referee.select_event.%d' % event.id)
                markup.add(winner)
            text = event.format(user.replicas, spec='referee', user=user)
            bot.send_message(user.id, text, reply_markup=markup)


def send_possible_cars_for_participation(user, event, text, origin_message=None):
    cars = user.race_cars.filter(created=True).all()
    markup = types.InlineKeyboardMarkup()
    possible_cars = 0
    reasons = []
    if cars.count() != 0:
        for car in cars:
            availability = event.can_participate(car)
            if availability.status:

                possible_cars += 1
                markup.add(types.InlineKeyboardButton('%s' % car.brand,
                                                      callback_data='find.participate_with_car.%d.%d' % (
                                                          event.id, car.id)))
            else:
                reasons += availability.messages
    else:
        reasons += event.can_participate(user=user).messages
    if possible_cars == 0:
        text = user.replicas.no_cars_message
        text += '\n\n' + user.replicas.reasons_text
        if cars.count() == 0:
            reasons.append(user.replicas.zero_cars)
        reasons = list(set(reasons))
        for reason in reasons:
            text += reason + '\n'

    if origin_message:
        bot.edit_message_text(text, user.id, origin_message.message_id, reply_markup=markup)
    else:
        bot.send_message(user.id, text, reply_markup=markup)
