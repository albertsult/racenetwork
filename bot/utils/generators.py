from messages import messages


def gen_car_text(user, cars):
    result = user.replicas.your_cars_text
    for car in cars:
        result += car.format(user.replicas)
    return result


def get_event_text(user, events):
    result = ''
    for event in events:
        race_type = messages[user.lang].round_race_text if event.race_type == 0 else messages[user.lang].drug_race_text
        result += messages[user.lang].event_format % (event.time.strftime('%H:%M %d.%m.%y'),
                                                      event.place,
                                                      event.input_sum,
                                                      event.max_participants,
                                                      race_type) + '\n\n'
    return result
