import logging

import telebot
from django.utils.log import AdminEmailHandler

from bot.controllers.bot.bot import existed_user_action, callback
from bot.controllers.user import get_user
from bot.utils.to_page_actions import ask_for_language
from bot.utils.utils import phone_format
from . import bot

bot_logger = logging.getLogger('bot_logic')
bot_logger.setLevel(logging.WARNING)
bot_logger.addHandler(logging.FileHandler('warning.logs'))


@bot.message_handler(commands=['start'])
def handle_start(message):
    ref_id = extract_unique_code(message.text)
    is_existed_user, user = get_user(message.from_user, ref_id)
    if is_existed_user:
        user.state = 0
        ask_for_language(user)
        user.save()


@bot.message_handler(content_types=['text', 'photo', 'document'])
def main_handler(message):
    is_existed_user, user = get_user(message.from_user)
    if is_existed_user:
        existed_user_action(user, message)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    is_existed_user, user = get_user(call.from_user)
    if is_existed_user:
        callback(call.message, call.data, user)


@bot.message_handler(content_types=['contact'])
def contact_handler(message):
    status, user = get_user(message.from_user)
    user.phone = phone_format(message.contact.phone_number)
    bot.send_message(user.id, user.replicas.contact_changed)
    user.save()


def extract_unique_code(text):
    # Extracts the unique_code from the sent /start command.
    return text.split()[1] if len(text.split()) > 1 else None