FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y --no-install-recommends gettext cron locales && apt-get clean && \
pip install uwsgi

RUN sed -i -e 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen && dpkg-reconfigure --frontend=noninteractive locales &&update-locale LANG=ru_RU.utf-8


WORKDIR /app

COPY ./requirements.txt .
RUN pip install -r requirements.txt --no-cache-dir

COPY . .

USER $MOD_WSGI_USER:$MOD_WSGI_GROUP


EXPOSE 8000
ENTRYPOINT ["/app/scripts/docker-entrypoint.sh"]
