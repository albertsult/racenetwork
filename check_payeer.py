import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "race_bot.settings")
django.setup()

from bot.controllers.payeer import run


if __name__ == '__main__':
    run()