#Non VCS files
db.env 

```
POSTGRES_DB=race_bot_db
POSTGRES_USER=race_bot_user
POSTGRES_PASSWORD=race_bot_temp
```

dev.env

```
HOST=<host name with port>
DEBUG=True
```

config.py

```python
# This file should not be presented in any public resources. Especially in Version Control system, like github.

token = '<token>'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = "<gmail login>"
EMAIL_HOST_PASSWORD = "<gmail pass>"
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False

payeer_account = '<>'
payeer_api_id = '<>'
payeer_api_pass = '<>'

payeer_income_account = '<>'
payeer_income_api_id = '<>'
payeer_income_api_pass = '<>'

telegram_file_link = 'https://api.telegram.org/file/bot%s/%s'
```